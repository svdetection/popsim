#!/usr/bin/env python3

import sys
import os
import shutil
import random
import argparse
import configparser
import re
import traceback
from collections import OrderedDict
import vcf
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import tempfile
from pysam import tabix_compress, tabix_index, VariantFile
from lib.variants_simulator import VariantsSimulator
from lib.exceptions import InputException, ExecException
import multiprocessing
import subprocess


class Print:
    def __init__(self, stdout=None, stderr=None, silent=False):
        self.stdout = stdout
        self.stderr = stderr
        self.silent = silent
        if self.stdout is not None and os.path.isfile(self.stdout):
            os.remove(self.stdout)
        if self.stderr is not None and os.path.isfile(self.stderr):
            os.remove(self.stderr)

    def out(self, message):
        if self.silent:
            return
        if self.stdout is not None:
            with open(self.stdout, "a") as stdout_f:
                print(message, file=stdout_f)
        else:
            print(message, file=sys.stdout)

    def err(self, message):
        if self.silent:
            return
        if self.stderr is not None:
            with open(self.stderr, "a") as stderr_f:
                print(message, file=stderr_f)
        else:
            print(message, file=sys.stderr)


def check_min_size(value):
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("%s is an invalid size value (>= 0)" % value)
    return ivalue


def parse_args(argv=None):
    """
    Parse script arguments
    :return: argparse arguments object
    """
    prg_path = os.path.dirname(os.path.realpath(__file__))

    # We try here to use a config file with the following behavior:
    # Start by using parse_known_args() to parse a configuration file
    # from the commandline, then read it with ConfigParser and set the
    # defaults, and then parse the rest of the options with parse_args().
    # This will allow you to have a default value, override that with a
    # configuration file and then override that with a commandline option
    # see https://stackoverflow.com/questions/3609852/which-is-the-best-way-to-allow-configuration-options-be-overridden-at-the-comman

    if argv is None:
        argv = sys.argv

    # Parse any conf_file specification
    # We make this parser with add_help=False so that
    # it doesn't parse -h and print help.
    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
        )
    conf_parser.add_argument("--conf-file",
                             help="Specify config file", metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()

    defaults = {'sv_list': prg_path + "/event_frequency.svlist",
                'nb_inds': 5,
                'coverage': 15,
                'output_dir': "res",
                'force_outputdir': False,
                'haploid': False,
                'force_polymorphism': False,
                'no-reads': False,
                'proba_del': 0.000001,
                'proba_dup': 0.000001,
                'proba_inv': 0.000001,
                'del_allele_freq': [0.2, 0.5],
                'dup_allele_freq': [0.2, 0.5],
                'inv_allele_freq': [0.2, 0.5],
                'read_len': 100,
                'insert_len_mean': 300,
                'insert_len_sd': 30,
                'quiet': False,
                'min_deletions': 0,
                'min_duplications': 0,
                'min_inversions': 0,
                'max_try': 10,
                'threads': 1  # multiprocessing.cpu_count()
                }

    if args.conf_file:
        config = configparser.SafeConfigParser()
        config.read([args.conf_file])
        # PATCH in order to convert a list-string toe a list of floats
        p = re.compile(r"\[(?P<freqs>[0-9\,\.\s]+)\]")
        defaults_dict = {}
        for k,v in config.items("Defaults"):
            if "freq" in k:
                m = p.match(v).group('freqs')
                values = [float(i) for i in m.split(",")]
                defaults_dict[k] = values
            else:
                defaults_dict[k] = v
        defaults.update(defaults_dict)
        # defaults.update(dict(config.items("Defaults")))

    # Parse rest of arguments
    # Don't suppress add_help here so it will handle -h
    parser = argparse.ArgumentParser(
        description='Generate simulated populations with SV',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        # Inherit options from config_parser
        parents=[conf_parser]
        )

    parser.add_argument("-n", "--nb-inds",
                        help="Number of individuals to generate", type=int)
    parser.add_argument("-r", "--reference",
                        help="Reference genome", required=True)
    parser.add_argument("-ns", "--nstretches",
                        help="N-stretches positions bed file", required=False)
    parser.add_argument("-s", "--sv-list",
                        help="File containing the SVs")
    parser.add_argument("-c", "--coverage", help="Coverage of reads", type=int)
    parser.add_argument("-o", "--output-dir", help="Output directory")
    parser.add_argument("-e", "--force-outputdir",
                        help="Delete output directory before start, "
                             "if already exists",
                        action='store_const', const=True)
    parser.add_argument("-f", "--force-polymorphism",
                        help="Force polymorphism for each SV",
                        action='store_const', const=True)
    parser.add_argument("-a", "--haploid",
                        help="Make a haploid genome, instead of diploid one",
                        action="store_const", const=True)
    parser.add_argument("--no-reads",
                        help="Only simulate rearranged genomes, do not simulate reads",
                        action="store_const", const=True)
    parser.add_argument("-pd", "--proba-del",
                        help="Probabilty to have a deletion", type=float)
    parser.add_argument("-pu", "--proba-dup",
                        help="Probabilty to have a duplication", type=float)
    parser.add_argument("-pi", "--proba-inv",
                        help="Probablity to have an insertion", type=float)
    parser.add_argument('-fd', '--del-allele-del', type=float, required=False,
                        help="Possible allele frequencies for deletions, space separated",
                        nargs="+")
    parser.add_argument('-fi', '--inv-allele-freq', type=float, required=False,
                        help="Possible allele frequencies for inversions, space separated",
                        nargs="+")
    parser.add_argument('-fu', '--dup-allele-freq', type=float, required=False,
                        help="Possible allele frequencies for duplications, space separated",
                        nargs="+") #MC
    parser.add_argument("-l", "--read-len",
                        help="Generate reads having a length of LEN", type=int)
    parser.add_argument("-m", "--insert-len-mean",
                        help="Generate inserts (fragments) having an average length of LEN",
                        type=int)
    parser.add_argument("-v", "--insert-len-sd",
                        help="Set the standard deviation of the insert (fragment) length (%%)",
                        type=int)
    parser.add_argument("-q", "--quiet",
                        help="Don't ask anything, choose default answer instead",
                        action="store_const", const=True)
    parser.add_argument("--silent",
                        help="No standard output",
                        action="store_const", const=True)
    parser.add_argument("-md", "--min-deletions",
                        help="Minimum of deletions to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("-mi", "--min-inversions",
                        help="Minimum of inversions to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("-mu", "--min-duplications",
                        help="Minimum of duplications to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("--max-try", help="Maximum of tries",
                        type=int)
    parser.add_argument("-t", "--threads",
                        help="Number of threads", type=int)
    parser.add_argument("-g", "--genotypes",
                        help="Position of SV with genotypes of individuals")
    #print(defaults)
    parser.set_defaults(**defaults)


    args = parser.parse_args(remaining_argv)

    if args.nb_inds is None and args.genotypes is None:
        parser.error("--nb-inds parameter is required (except if --genotypes parameter is given)")

    if args.proba_del > 1 or args.proba_del < 0:
        parser.error("Probability of deletions must be between 0 and 1")

    if args.proba_inv > 1 or args.proba_inv < 0:
        parser.error("Probability of inversions must be between 0 and 1")

    if args.proba_dup > 1 or args.proba_dup < 0:
        parser.error("Probability of duplication must be between 0 and 1")

    if args.proba_del + args.proba_inv + args.proba_dup > 1:
        parser.error("Sum of inversions/deletions/duplications probabilities must be lesser than 1")

    return args


def _allele(frequency):
    """
    Get randomly an allele, given a frequency
    :param frequency: frequency of the allele {float}
    :return: allele randomly choosen {0 or 1}
    """
    return 1 if random.uniform(0, 1) < frequency else 0


def _get_genotypes_for_inds(nb_inds, haploid, freq):
    """
    Get genotypes for each individual
    :param nb_inds: number of individuals {int}
    :param haploid: is the genome hamploid {bool}
    :param freq: frequency of the allele {float}
    :return: list of genotypes, vcf data {list}
    """
    all_genotypes = []
    genotypes_row = []
    for i in range(1, nb_inds + 1):
        if not haploid:
            genotype = str(_allele(freq)) + "/" + str(_allele(freq))
        else:
            genotype = str(_allele(freq))
        genotype_data = vcf.model._Call(None, "indiv" + str(i), vcf.model.make_calldata_tuple("GT")(GT=genotype))
        genotypes_row.append(genotype_data)
        all_genotypes.append(genotype)
    return all_genotypes, genotypes_row


def _svsort(sv, chrm, genotypes_for_inds):
    """
    Function to sort regions
    :param sv: the variant
    :param chrm: chromosome {str}
    :param genotypes_for_inds: dictionary of genotypes for each individual
    """
    return int(genotypes_for_inds[chrm][sv]["start"])


def _build_vcf_header(vcf_file, prg_path, tmp_dir, nb_inds):
    """
    Build header of the VCF file
    :param vcf_file: vcf file full path name {str}
    :param prg_path: program path {str}
    :param tmp_dir: temporary directory {str}
    :param nb_inds: number of individuals {int}
    """
    try:
        with open(os.path.join(prg_path, "template.vcf"), "r") as template:
            try:
                with open(vcf_file, "w") as my_template:
                    for line in template:
                        if line[:6] == "#CHROM":
                            line = line.replace("\n", "")
                            for i in range(0, nb_inds):
                                line += "\tindiv" + str(i+1)
                            line += "\n"
                        my_template.write(line)
            except IOError:
                raise ExecException("ERROR: unable to create template file \"{0}\" in temp dir.".
                                format(os.path.join(tmp_dir, "template.vcf")))
    except IOError:
        raise ExecException("ERROR: template file not found in program directory.")


def build_genotypes_vcf_list(deletions: dict,
                             inversions: dict,
                             duplications: dict,
                             output_vcf, haploid,
                             force_polymorphism, nb_inds,
                             tmp_dir, prg_path):
    """
    Build VCF file describing genotypes for each individual (and the associated python dictionary)
    :param deletions: deletions description {dict}
    :param inversions: inversions description {dict}
    :param duplications: duplications description {dict}
    :param output_vcf: output VCF file full path name {str}
    :param haploid: is haploid {bool}
    :param force_polymorphism: force polymorphism {bool}
    :param nb_inds: number of individuals {int}
    :param tmp_dir: temporary directory {str}
    :param prg_path: program path {str}
    :return: dictionary of genotypes for each variant for each dictionary {OrderedDict}
    """

    vcf_file = os.path.join(tmp_dir, "template.vcf")

    # Build VCF header:
    _build_vcf_header(vcf_file, prg_path, tmp_dir, nb_inds)

    records = []

    # Deletions:
    genotypes_for_inds_DEL = OrderedDict()
    # { chr: { id_indiv: {start: #start, end: #end, genotypes: [[0,1],[1,1],...], ...}, # ...}

    for chrm, deletes in deletions.items():
        for delete in deletes:
            if chrm not in genotypes_for_inds_DEL:
                genotypes_for_inds_DEL[chrm] = {}
            genotypes_for_inds_DEL[chrm][delete["name"]] = {"start": delete["start"], "end": delete["end"], "genotypes": []}

            # Get genotypes:
            all_genotypes, genotypes = [], []
            if force_polymorphism:
                polymorph = False
                while not polymorph:
                    all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, delete["allele_freq"])
                    polymorph = len(set(all_genotypes)) > 1
            else:
                all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, delete["allele_freq"])
            genotypes_for_inds_DEL[chrm][delete["name"]]["genotypes"] = [x.split("/") for x in all_genotypes]

            info = {"END": delete["end"], "AF": delete["allele_freq"]}
            vcf_record = vcf.model._Record(chrm, delete["start"], delete["name"], "N", [vcf.model._SV("DEL")], ".", ##start-1 because 0-based?
                                           ".", info, "GT", [0], genotypes)
            records.append(vcf_record)

    # Inversions:
    genotypes_for_inds_INV = OrderedDict()
    for chrm, the_inversions in inversions.items():
        for inversion in the_inversions:
            if chrm not in genotypes_for_inds_INV:
                genotypes_for_inds_INV[chrm] = {}
            genotypes_for_inds_INV[chrm][inversion["name"]] = {"start": inversion["start"], "end": inversion["end"],
                                                           "genotypes": []}
            # Get genotypes:
            all_genotypes, genotypes = [], []
            if force_polymorphism:
                polymorph = False
                while not polymorph:
                    all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, inversion["allele_freq"])
                    polymorph = len(set(all_genotypes)) > 1
            else:
                all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, inversion["allele_freq"])
            genotypes_for_inds_INV[chrm][inversion["name"]]["genotypes"] = [x.split("/") for x in all_genotypes]

            info = {"END": inversion["end"], "AF": inversion["allele_freq"]}
            vcf_record = vcf.model._Record(chrm, inversion["start"], inversion["name"], "N", [vcf.model._SV("INV")], ## -1 for 0-based? or already done by vcf model?
                                           ".", ".", info, "GT", [0], genotypes)
            records.append(vcf_record)

    # Duplications: <MC>
    genotypes_for_inds_DUP = OrderedDict()
    for chrm, the_duplications in duplications.items():
        for duplication in the_duplications:
            if chrm not in genotypes_for_inds_DUP:
                genotypes_for_inds_DUP[chrm] = {}
            genotypes_for_inds_DUP[chrm][duplication["name"]] = {"start": duplication["start"], "end": duplication["end"],"genotypes": [], "shift": duplication["shift"]}
            # Get genotypes:
            all_genotypes, genotypes = [], []
            if force_polymorphism:
                polymorph = False
                while not polymorph:
                    all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, duplication["freq"])
                    polymorph = len(set(all_genotypes)) > 1
            else:
                all_genotypes, genotypes = _get_genotypes_for_inds(nb_inds, haploid, duplication["freq"])
            genotypes_for_inds_DUP[chrm][duplication["name"]]["genotypes"] = [x.split("/") for x in all_genotypes]

            info = {"END": duplication["end"], "AF": duplication["freq"],"DS":duplication["shift"]}
            vcf_record = vcf.model._Record(chrm, duplication["start"], duplication["name"], "N", [vcf.model._SV("DUP")], ## -1 for 0-based? or already done by vcf model?
                                           ".", ".", info, "GT", [0], genotypes)
            records.append(vcf_record) #<MC>

    records.sort(key=lambda r: (r.CHROM, r.start))

    with open(output_vcf, "w") as o_vcf:
        vcf_reader = vcf.Reader(filename=vcf_file)
        vcf_writer = vcf.Writer(o_vcf, vcf_reader)
        for record in records:
            vcf_writer.write_record(record)
        vcf_writer.close()

    tabix_compress(output_vcf, output_vcf + ".gz", True)
    tabix_index(output_vcf + ".gz", force=True, preset="vcf")

    os.remove(output_vcf)

    return genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP


def load_genotypes_from_file(genotypes_file):
    variants = VariantFile(genotypes_file)
    genotypes_for_inds_DEL = {}
    genotypes_for_inds_INV = {}
    genotypes_for_inds_DUP = {}
    nb_inds = 0
    for variant in variants:
        chrm = variant.chrom
        id_v = variant.id
        start = variant.start
        end = variant.stop
        type_v = variant.alts[0][1:-1]
        samples = variant.samples
        gt = [list(map(str, samples[x]["GT"])) for x in samples]
        genotypes_for_inds = None
        if type_v == "DEL":
            genotypes_for_inds = genotypes_for_inds_DEL
        elif type_v == "INV":
            genotypes_for_inds = genotypes_for_inds_INV
        elif type_v == "DUP":
            genotypes_for_inds = genotypes_for_inds_DUP
        else:
            print("ERROR: Variant type not supported: %s. Ignoring this line..." % type_v)
        if genotypes_for_inds is not None:
            if chrm not in genotypes_for_inds:
                genotypes_for_inds[chrm] = {}
            genotypes_for_inds[chrm][id_v] = {
                "start": start,
                "end": end,
                "genotypes": gt
            }
            nb_inds = len(gt)
    return nb_inds, genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP


def _compute_keeped_genomic_regions(svs, svs_infos, haploid):
    """
    Get list of all regions keeped (not deleted)
    :param svs: list of variants (deletions) {list}
    :param svs_infos: infos of variants {dict}
    :param haploid: is haploid {bool}
    :return: regions for each individuals {OrderedDict}, the last position for each region {OrderedDict}
    """
    regions = OrderedDict()
    current_region_pointer = OrderedDict()
    for svs_i in svs:
        i = 0
        for genotypes in svs_infos[svs_i]["genotypes"]:
            if i not in regions:
                regions[i] = {}
                current_region_pointer[i] = {}
            for j in range(0, 2 if not haploid else 1):  # For each chromosome of the diploid genome, or the chromosome
                # of the haploid genome
                if j not in regions[i]:
                    regions[i][j] = []
                    current_region_pointer[i][j] = "0"
                if svs_infos[svs_i]["genotypes"][i][j] == "1":
                    regions[i][j].append([current_region_pointer[i][j], svs_infos[svs_i]["start"]])
                    current_region_pointer[i][j] = svs_infos[svs_i]["end"]
            i += 1
    return regions, current_region_pointer


def _build_fastas_deletions(chrm, regions, current_region_pointer, output_dir, fasta_orig_chr, last_nt):
    """
    Build fasta files
    :param chrm:
    :param regions:
    :param current_region_pointer:
    :param output_dir:
    :param fasta_orig_chr:
    :param last_nt:
    :return:
    """
    n = 0
    fastas = []
    for indiv, chrs_dips in regions.items():
        id_chrs = list(chrs_dips.keys())
        id_chrs = sorted(id_chrs)
        c = 0
        fastas.append([])
        for id_chr in id_chrs:
            chr_dip = chrs_dips[id_chr]  # SVs for each diploid chromosome
            logs_regions = []  # Store logs

            # Build FASTA and store logs:

            fasta = ""
            last_one = 0
            for chr_region in chr_dip:
                fasta += fasta_orig_chr[n][c][int(chr_region[0]):int(chr_region[1])]
                logs_regions.append("\t".join(str(x) for x in chr_region))
                last_one = int(chr_region[1])
            if last_one < last_nt:
                logs_regions.append("\t".join([str(current_region_pointer[indiv][id_chr]), str(last_nt)]))
                fasta += fasta_orig_chr[n][c][int(current_region_pointer[indiv][id_chr]):last_nt]
            fastas[n].append(fasta)

            # Write logs
            try:
                with open(os.path.join(output_dir, "indiv" + str(indiv + 1) + ".regions.log"), "a") as log_handle:
                    log_handle.write(chrm + "_" + str(id_chr) + "\t")
                    log_handle.write("\n\t".join(logs_regions))
                    log_handle.write("\n")
            except IOError:
                raise ExecException("ERROR: unable to write log file: \"{0}\"".
                                    format(os.path.join(output_dir, "indiv" + str(indiv + 1) + ".regions.log")))
            c += 1
        n += 1
    return fastas


def _build_fastas_inversions(fasta_orig_chrm: dict, genotypes_for_inds: dict, nb_inds, haploid):
    fastas = []
    for j in range(0, nb_inds):
        fastas.append([fasta_orig_chrm] if haploid else [fasta_orig_chrm, fasta_orig_chrm])
    for inv_name, props in genotypes_for_inds.items():
        i = 0
        for genotype in props["genotypes"]:
            for k in range(0, len(genotype)):
                if genotype[k] == "1":
                    start = int(props["start"])
                    end = int(props["end"])
                    o_id = fastas[i][k].id
                    o_name = fastas[i][k].name
                    o_description = fastas[i][k].description
                    fastas[i][k] = fastas[i][k][:start] + fastas[i][k][start:end].reverse_complement() + \
                                   fastas[i][k][end:]
                    fastas[i][k].id = o_id
                    fastas[i][k].name = o_name
                    fastas[i][k].description = o_description
            i += 1
    return fastas


def _write_fastas(fastas, output_dir):
    for indiv, chrs_dips in zip(range(0, len(fastas)), fastas):
        for id_chr, fasta in zip(range(0, len(chrs_dips)), chrs_dips):
            try:
                with open(os.path.join(output_dir, "indiv" + str(indiv + 1) + "_chr_" + str(id_chr) + ".fasta"),
                          "a") as output_handle:
                    SeqIO.write(fasta, output_handle, "fasta")
            except IOError:
                raise ExecException("ERROR: unable to write \"{0}\" file.".
                                    format(os.path.join(output_dir, "indiv" + str(indiv + 1) + "_chr_" + str(id_chr) +
                                                        ".fasta")))


def build_fastas_chromosomes(reference, genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP, haploid, output_dir, nb_inds,
                             printer):
    printer.out("BUILD FASTA GENOME FOR EACH INDIVIDUAL...\n")
    fasta_orig = SeqIO.index(reference, "fasta")

    ### Building the list of genome modification, with the sequence of allele 0 and allele 1
    fasta_modif_list = OrderedDict()

    for chrm, svs_infos in genotypes_for_inds_INV.items():
        if chrm not in fasta_modif_list.keys():
            fasta_modif_list[chrm]=[]
        for sv in svs_infos:
            fasta_modif_list[chrm].append(build_info_inversion_from_sv(svs_infos[sv],fasta_orig[chrm]))

    for chrm, svs_infos in genotypes_for_inds_DUP.items():
        if chrm not in fasta_modif_list.keys():
            fasta_modif_list[chrm]=[]
        for sv in svs_infos:
            fasta_modif_list[chrm].append(build_info_duplication_from_sv(svs_infos[sv],fasta_orig[chrm]))

    for chrm, svs_infos in genotypes_for_inds_DEL.items():
        if chrm not in fasta_modif_list.keys():
            fasta_modif_list[chrm]=[]
        for sv in svs_infos:
            fasta_modif_list[chrm].append(build_info_deletion_from_sv(svs_infos[sv],fasta_orig[chrm]))

    #print("List",fasta_modif_list)
    ###

    ### Building the complete sequence list, adding the common part between the modification and their correspondnig sequence
    ## TODO : do it in one step
    fasta_frag={}

    for chrm in fasta_orig:
        last_nt =  len(fasta_orig[chrm])
        fasta_frag_chrm=[]
        previous_end=0
        if chrm not in fasta_modif_list.keys():
            fasta_frag_chrm.append(build_info_common(previous_end+1,last_nt,fasta_orig[chrm],nb_inds)) ## test_seq
        else:
            svariants = sorted(fasta_modif_list[chrm], key=lambda x: x["start"])
            for sv in svariants:
                if sv['start']-previous_end > 1:
                    fasta_frag_chrm.append(build_info_common(previous_end+1,sv['start']-1,fasta_orig[chrm],nb_inds)) ## test_seq
                    fasta_frag_chrm.append(sv)
                    previous_end=sv['end']
                else:
                    fasta_frag_chrm.append(sv)
                    previous_end=sv['end']

            if fasta_frag_chrm[-1]['end'] < last_nt:
                fasta_frag_chrm.append(build_info_common(fasta_frag_chrm[-1]['end']+1,last_nt,fasta_orig[chrm],nb_inds)) ## test_seq

        fasta_frag[chrm] = fasta_frag_chrm
        # for t in fasta_frag[chrm]:
        #     print(t['type'],t['start'],t['end'],t['genotypes'])
    ###

    ### fasta buildings
    ### use the complete list to create the haplotype sequence
    ### for each haplotype, we append the common sequence and the allele sequence corresponding to the haplotype (0 or 1)
    ## TOSO : it's a bit slow, try to fasten the process
    for chrm in fasta_orig:
        for ind in range(0, nb_inds):
            for haplo in [0,1]:
                seq_haplo_ind=SeqRecord(Seq(''),id=fasta_orig[chrm].id, name=fasta_orig[chrm].name, description=fasta_orig[chrm].description)
                for sv in fasta_frag[chrm]:
                    geno_haplo = int(sv['genotypes'][ind][haplo])
                    seq_haplo_ind += sv['seq'][geno_haplo]

                seq_haplo_ind.id=fasta_orig[chrm].id
                seq_haplo_ind.name=fasta_orig[chrm].name
                seq_haplo_ind.description=fasta_orig[chrm].description
                output_handle = open(os.path.join(output_dir, "indiv" + str(ind + 1) + "_chr_" + str(haplo) + ".fasta"),"w")
                SeqIO.write(seq_haplo_ind, output_handle, "fasta")

    ###


def build_info_common(start,end,fasta_orig_chrm,nb_inds):
    info={}
    info['type']='COMMON'
    info['start']=start
    info['end']=end
    info['size']=info['end'] - info['start'] + 1
    info['genotypes']=[]
    for ind in range(0, nb_inds):
        info['genotypes'].append(['0','0'])

    seq_ori = fasta_orig_chrm[int(info['start']-1):int(info['end'])]
    info['seq']=[seq_ori,seq_ori]
    return info

def build_info_deletion_from_sv(sv,fasta_orig_chrm):
    ## TODO : should probably shift the sequence by 1 to fit vcf coding of the deletion
    info={}
    info['type']='DELETION'
    info['start']=int(sv['start'])
    info['end']=int(sv['end'])
    info['size']=info['end'] - info['start'] + 1
    info['genotypes']=sv['genotypes']
    seq_ori = fasta_orig_chrm[int(sv['start']-1):int(sv['end'])]
    empty = SeqRecord(Seq(''),id=seq_ori.id, name=seq_ori.name, description=seq_ori.description)
    info['seq']=[seq_ori,empty]
    return info

def build_info_inversion_from_sv(sv,fasta_orig_chrm):
    info={}
    info['type']='INVERSION'
    info['start']=int(sv['start'])
    info['end']=int(sv['end'])
    info['size']=info['end'] - info['start'] + 1
    info['genotypes']=sv['genotypes']
    seq_ori = fasta_orig_chrm[int(sv['start']-1):int(sv['end'])]
    info['seq']=[seq_ori,seq_ori.reverse_complement(id=seq_ori.id,name=seq_ori.name,description=seq_ori.description)]
    return info

def build_info_duplication_from_sv(sv,fasta_orig_chrm,reverse=False):

    info={}
    info['type']='DUPLICATION'

    ## The CNV will be represented by the target_base (base after which the duplicated sequence insert)
    ## 0 : target_base, 1 : target base + duplicated sequence
    ## beware1 : if target base > seq_length, the insertion will occur at the end of the sequence
    ## beware2 : if multiple duplications are inserted at the end of the sequence, the target site will be duplicated !!
    ## => patch : check if multiple duplication insert in the same place and keep only once the target_base
    ## => not a priority

    target_base_pos = int(sv['end']+sv['shift'])
    seq_ori = fasta_orig_chrm[int(sv['start']-1):int(sv['end'])]
    target_base = fasta_orig_chrm[target_base_pos-1:target_base_pos]

    info['start']=target_base_pos
    info['end']=target_base_pos
    info['size']=sv['end']-sv['start'] + 1 #size of duplicated sequence
    info['genotypes']=sv['genotypes']

    if reverse:
        info['seq']=[target_base,target_base + seq_ori.reverse_complement(id=seq_ori.id,name=seq_ori.name,description=seq_ori.description)]
    else:
        info['seq']=[target_base,target_base + seq_ori]
    return info


def generate_samples_fastq(haploid, nb_inds, output_dir, coverage, read_len, insert_len_mean, insert_len_sd,
                           prg_path, threads, stdout, stderr):
    # Generate reads for all individuals:
    cmd = [prg_path + "/pirs/pirs", "simulate", "-z", "-x", str(coverage), "-d", "-B",
           prg_path + "/pirs/Profiles/Base-Calling_Profiles/humNew.PE100.matrix.gz", "-I",
           prg_path + "/pirs/Profiles/InDel_Profiles/phixv2.InDel.matrix", "-l", str(read_len), "-m",
           str(insert_len_mean), "-v", str(insert_len_sd),
           "-G", prg_path + "/pirs/Profiles/GC-depth_Profiles/humNew.gcdep_100.dat", "-t", str(threads), "-o"]
    if haploid:
        cmd.remove("-d")
    for i in range(1, nb_inds+1):
        cmd_full = cmd + [output_dir, "-s", "indiv%d" % i]
        prefix_fasta = os.path.join(output_dir, "indiv" + str(i))
        chr0 = prefix_fasta + "_chr_0.fasta"
        if not haploid:
            chr1 = prefix_fasta + "_chr_1.fasta"
            cmd_full += [chr0, chr1]
        else:
            cmd_full.append(chr0)
        subprocess.call(cmd_full, stdout=open(stdout, "a") if stdout is not None else None,
                        stderr=open(stderr, "a") if stderr is not None else None)
        print("#".join(cmd_full))


def confirm(deletions: dict, inversions: dict, duplications: dict,variants: dict, printer: Print, ask=True):
    # Deletions:
    nb_dels = 0
    if "DEL" in variants:
        variants_del = sorted(variants["DEL"], key=lambda x: x["min"])
        variants_del_counts = OrderedDict()
        variants_del_counts_chr = {}
        for variant in variants_del:
            variants_del_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, deletes in deletions.items():
            nb_dels_chr = len(deletes)
            nb_dels += nb_dels_chr
            variants_del_counts_chr[chrm] = nb_dels_chr
            for delete in deletes:
                v_len = delete["length"]
                for variant in variants_del:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_del_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("We generate {0} deletion variants.".format(nb_dels))
        if nb_dels > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_del_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_del_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_del_counts_chr[chrm]))
        printer.out("")

    # Inversions:
    nb_invs = 0
    if "INV" in variants:
        variants_inv = sorted(variants["INV"], key=lambda x: x["min"])
        variants_inv_counts = OrderedDict()
        variants_inv_counts_chr = {}
        for variant in variants_inv:
            variants_inv_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, the_inversions in inversions.items():
            nb_invs_chr = len(the_inversions)
            nb_invs += nb_invs_chr
            variants_inv_counts_chr[chrm] = nb_invs_chr
            for inversion in the_inversions:
                v_len = inversion["length"]
                for variant in variants_inv:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_inv_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("We generate {0} inversion variants.".format(nb_invs))
        if nb_invs > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_inv_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_inv_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_inv_counts_chr[chrm]))
        printer.out("")

    # Duplications: #<MC>
    nb_dups = 0
    if "DUP" in variants:
        variants_dup = sorted(variants["DUP"], key=lambda x: x["min"])
        variants_dup_counts = OrderedDict()
        variants_dup_counts_chr = {}
        for variant in variants_dup:
            variants_dup_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, the_duplications in duplications.items():
            nb_dups_chr = len(the_duplications)
            nb_dups += nb_dups_chr
            variants_dup_counts_chr[chrm] = nb_dups_chr
            for duplication in the_duplications:
                v_len = duplication["length"]
                for variant in variants_dup:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_dup_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("We generate {0} duplication variants.".format(nb_dups))
        if nb_dups > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_dup_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_dup_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_dup_counts_chr[chrm]))
        printer.out("") #</MC>

    # Confirm:
    if ask:
        return input("Continue [Y/n]? ") in ["y", "Y", ""]
    return True


def init(output_dir, force_outputdir, sv_list, nstretches, nb_inds, reference, proba_del, proba_inv, proba_dup, del_allele_freq, inv_allele_freq, dup_allele_freq,
         haploid, force_polymorphism, no_reads, coverage, read_len, insert_len_mean, insert_len_sd, threads, genotypes=None,
         min_deletions=1, min_inversions=1, min_duplications=1, max_try=20, quiet=True, stdout=None, stderr=None, silent=False): #MC

    printer = Print(stdout=stdout, stderr=stderr, silent=silent)

    if os.path.isdir(output_dir):
        if force_outputdir:
            shutil.rmtree(output_dir)
        else:
            printer.err("Error: output directory {0} already exists.".format(output_dir))
            return 1
    elif os.path.isfile(output_dir):
        printer.err("Error: unable to create output directory {0}: file exists.".format(output_dir))
        return 1

    if nb_inds is not None and nb_inds < 2:
        printer.err("nb-inds must be at least 2")
        return 1

    if not os.path.isfile(reference + ".fai"):
        os.system("samtools faidx " + reference)

    tmp_dir = tempfile.mkdtemp()

    prg_path = os.path.dirname(os.path.realpath(__file__))

    os.mkdir(output_dir)

    ############################
    # Define fixed files names #
    ############################
    output_vcf = os.path.join(output_dir, "genotypes.vcf")

    ################
    # Launch steps #
    ################

    deletions = None
    inversions = None
    duplications = None #MC
    sv_sim = None

    if genotypes is None:
        printer.out("GENERATE RANDOM DELETIONS VARIANTS...\n")
        try:
            nb_deletions = -1  # To allow 0 deletions
            nb_inversions = -1  # To allow 0 inversions
            nb_duplications = -1  # To allow 0 inversions #MC
            nb_try = 0
            # print("sv_list",sv_list)
            # print("nstretches",nstretches)
            # print("threads",threads)
            # print("del_allele_freq",del_allele_freq)
            # print("inv_allele_freq",inv_allele_freq)
            # print("dup_allele_freq",dup_allele_freq)
            sv_sim = VariantsSimulator(sv_list=sv_list, nstretches=nstretches, threads=threads, stdout=stdout,
                                       stderr=stderr, del_allele_freq=del_allele_freq, inv_allele_freq=inv_allele_freq, dup_allele_freq=dup_allele_freq, silent=silent) #MC
            if "DEL" not in sv_sim.variants:
                proba_del = 0
            if "INV" not in sv_sim.variants:
                proba_inv = 0
            if "DUP" not in sv_sim.variants:
                proba_dup = 0 #MC
            printer.out("Try to generate at least %s deletions and %s inversions..." % (min_deletions, min_inversions))
            max_del = 0
            max_inv = 0
            max_dup = 0 #MC

            while (nb_deletions < min_deletions or nb_inversions < min_inversions or nb_duplications < min_duplications) and nb_try < max_try: #MC
                printer.out("\nTry {0} / {1}...".format(nb_try + 1, max_try))
                nb_deletions, deletions, nb_inversions, inversions, nb_duplications, duplications = sv_sim.get_random_variants(proba_del, proba_inv, proba_dup, reference) #MC
                max_del = max(max_del, nb_deletions)
                max_inv = max(max_inv, nb_inversions)
                max_dup = max(max_dup, nb_duplications) #MC
                sv_sim.print_flush()
                nb_try += 1
            if nb_deletions < min_deletions:
                printer.err("\nUnable to generate %s deletions. Try to reduce size of deletions or increase "
                                     "general probability to have a deletion." % min_deletions)
                printer.out("\nMaximum of each event: %d deletions, %d inversions, %d duplications." % (max_del, max_inv, max_dup)) #MC
                return 1
            if nb_inversions < min_inversions:
                printer.err("\nUnable to generate %s inversions. Try to reduce size of inversions or increase "
                            "general probability to have an inversion." % min_inversions)
                printer.out("\nMaximum of each event: %d deletions, %d inversions, %d duplications." % (max_del, max_inv, max_dup)) #MC
                return 1 #MC
            if nb_duplications < min_duplications: #MC
                printer.err("\nUnable to generate %s duplications. Try to reduce size of duplications or increase " #MC
                            "general probability to have a duplication." % min_duplications) #MC
                printer.out("\nMaximum of each event: %d deletions, %d inversions, %d duplications." % (max_del, max_inv, max_dup)) #MC
                return 1 #MC
            printer.out("")
        except InputException as e:
            printer.err(e)
            return 1
        except IOError as e:
            printer.err(e)
            return 1
        except Exception:
            printer.err(traceback.format_exc())
            return 1
        printer_confirm = printer
        ok = True
        if genotypes is None:
            if quiet:
                printer_confirm = Print(stdout=os.path.join(output_dir, "confirm.txt"), stderr=stderr)
                ok = confirm(deletions, inversions, duplications, sv_sim.variants, printer_confirm, False) #MC
            else:
                ok = confirm(deletions, inversions, duplications, sv_sim.variants, printer_confirm) #MC
    else:
        ok = True
    if ok:
        try:
            if genotypes is None:
                printer.out("GENERATE SUMMARY VCF FILE...\n")
                genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP = \
                    build_genotypes_vcf_list(deletions, inversions, duplications, output_vcf, haploid, force_polymorphism, nb_inds, #MC
                                             tmp_dir, prg_path)
            else:
                nb_inds, genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP = load_genotypes_from_file(genotypes) #MC
                if nb_inds < 2:
                    printer.err("nb-inds must be at least 2")
                    return 1
            build_fastas_chromosomes(reference, genotypes_for_inds_DEL, genotypes_for_inds_INV, genotypes_for_inds_DUP, haploid, output_dir,
                                     nb_inds, printer) #MC
            if not no_reads:
                printer.out("GENERATE RANDOM READS FOR EACH INDIVIDUAL FROM GENOME...\n")
                generate_samples_fastq(haploid, nb_inds, output_dir, coverage, read_len, insert_len_mean,
                                       insert_len_sd, prg_path, threads, stdout, stderr)
            printer.out("DONE!\n")
        except ExecException as e:
            printer.err(e)
            return 1
        except Exception:
            printer.err(traceback.format_exc())
            return 1
    else:
        printer.out("Aborted!\n")

    return 0


def main():
    args = parse_args()
    # for k, v in vars(args).items():
    #     print(k, v)
    # return
    init(**{k: v for k, v in vars(args).items() if k in init.__code__.co_varnames})


if __name__ == '__main__':
    sys.exit(main())
