#!/usr/bin/env python3

import sys
import os
import argparse

from collections import defaultdict
from pysam import VariantFile

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

MOTHER = 0
FATHER = 1


class Locus():
    def __init__(self, ref_frag, variant=None):
        self.chrom = ref_frag.chrom
        self.start = ref_frag.start
        self.end = ref_frag.end
        self.region = ref_frag.region
        self.id = self.region
        self.variants = [ref_frag]
        self.variant_proba = 0
        if variant is not None:
            self.variants.append(variant)
            self.variant_proba = variant.allele_freq

    @property
    def locus_type(self):
        return "variable" if len(self.variants) > 1 else "common"

    @property
    def reference(self):
        return self.variants[0]

    @property
    def alternate(self):
        if self.is_common:
            print("Error, not a variant locus %s", str(self))
            exit(1)
        return self.variants[1]

    @property
    def is_common(self):
        return 1 if len(self.variants) == 1 else 0

    def __str__(self):
        return "%s\t%d\t%d\t%-9s" % (self.chrom, self.start, self.end,
                                     self.locus_type)


class VariantGraph():
    def __init__(self, chromosome):
        self.chromosome = chromosome
        self.loci = []

    def reflength(self):
        return len(self.chromosome)

    @property
    def numloci(self):
        return len(self.loci)

    def new_locus(self, locus):
        self.loci.append(locus)

    def traversal(self):
        i = 0
        while i < self.numloci:
            yield self.loci[i]
            i += 1

    def print(self):
        for l in self.loci:
            print(l)


class Fragment():
    """
    A fragment is a chromosomal segment with start and end
    1-based for compatibility wit vcf format
    """
    def __init__(self, chrom, start, end):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.type = "REF"
        self.region = "%s:%d-%d" % (chrom, start, end)
        self.id = self.region

    def size(self):
        return self.end - self.start + 1

    def get_seq(self, reference):
        return reference[self.start-1:self.end]

    def __str__(self):
        return "%s\t%d\t%d\t%s" % (self.chrom, self.start, self.end, self.type)


class Variant(Fragment):
    def __init__(self, chrom, start, end, ident, allele_freq, type):
        Fragment.__init__(self, chrom, start, end)
        self.id = ident
        self.type = "ALT"
        self.svtype = type
        self.allele_freq = float(allele_freq)

    def get_svinfos(self):
        return (self.chrom, self.start, self.end, self.id, self.svtype)

    def get_len(self):
        return self.end - self.start + 1

    def get_seq(self, reference):
        if self.svtype == "DEL":
            return ""
        elif self.svtype == "INV":
            return reverse(complement(reference[self.start-1:self.end]))
        elif self.svtype == "DUP":
            template = reference[self.start-1:self.end]
            return template + template
        else:
            print("Unknown sv type %s" % self.svtype)

    def __str__(self):
        return "%s\t%d\t%d\t%3.2f\t%s" % (self.chrom, self.start, self.end,
                                          self.allele_freq, self.svtype)


class VariantPopulation():
    def __init__(self, samples):
        self.samples = samples
        self.sample_dict = {s.ident:s for s in samples}

    @property
    def num_samples(self):
        return len(self.samples)

    def get_sample(self, ident):
        if ident not in self.sample_dict:
            print("%s not a valid sample")
        return self.sample_dict[s]

    def ordered_samples(self):
        ordered = sorted(self.samples, key=lambda x:x.ident)
        return ordered

    def close_filehandles(self):
        for sample in self.samples:
            sample.close_filehandles()


class VariantSample():
    def __init__(self, ident, outdir):
        self.ident = ident
        self.genotypes = defaultdict(list)
        self.maternal = Haplotype(ident, "maternal", outdir)
        self.paternal = Haplotype(ident, "paternal", outdir)

    def clear_haplotypes(self):
        self.maternal.clear_haplotype()
        self.paternal.clear_haplotype()

    def get_genotype_str(self, locus):
        if locus.id not in self.genotypes:
            print("%s not a vvalid locus" % locus.id)
            exit(1)
        return "%d|%d" % tuple(self.genotypes[locus.id])

    def get_allgenotypes_str(self):
        genos = []
        for genotype in self.genotypes.values():
            genos.append("%d|%d" % tuple(genotype))
        return "\t".join(genos)

    def get_genotypes(self, vg):
        genotypes = []
        for locus in vg.traversal():
            locus_genos = self.genotypes[locus.id]
            genotypes.append("%d/%d" % (locus_genos[0], locus_genos[1]))
        return "%s\t%s" % (self.ident, " ".join(genotypes))

    def display_haplotypes(self):
        print(self.ident)
        for fragment in self.maternal.fragments:
            print(fragment)
        for fragment in self.paternal.fragments:
            print(fragment)

    def dump_chrom_fastas(self, chrom, vg, reference):
        self.clear_haplotypes()
        for locus in vg.traversal():
            mat_geno, pat_geno = self.genotypes[locus.id]
            self.maternal.extend_haplotype(locus.variants[mat_geno])
            self.paternal.extend_haplotype(locus.variants[pat_geno])
        self.maternal.dump_chrom_fasta(chrom, reference)
        self.paternal.dump_chrom_fasta(chrom, reference)

    def close_filehandles(self):
        self.maternal.close_filehandle()
        self.paternal.close_filehandle()

    def __str__(self):
        return "%s\t%s" % (self.ident, self.get_allgenotypes_str())


class Haplotype():
    def __init__(self, ident, origin, outdir):
        self.ident = ident
        self.code = 0 if origin == "maternal" else 1
        self.filename = os.path.join(outdir,
                                     "%s_chr_%d.fasta" % (ident, self.code))
        self.output_handle = None
        self.origin = origin
        self.fragments = []

    def clear_haplotype(self):
        self.fragments = []

    def get_fragments(self):
        return self.fragments

    def extend_haplotype(self, fragment):
        # TODO execution time might be improved if consecutive reference
        # fragment are merged
        # For example : if top of pile and successor are ref :
        # we merge, hence extend reference fragment (not trivial)
        self.fragments.append(fragment)

    def get_haplotype_seq(self, ref_seq):
        return "".join([fra.get_seq(ref_seq) for fra in self.fragments])

    def dump_chrom_fasta(self, chrom, ref_seq):
        if not self.output_handle:
            self.output_handle = open(self.filename, "w")
        seq_str = self.get_haplotype_seq(ref_seq)
        bioseq = Seq(seq_str)
        chrom_seq = SeqRecord(bioseq, id=chrom,
                              description="simulated haplotype %s for %s"
                              % (self.origin, self.ident))
        SeqIO.write(chrom_seq, self.output_handle, "fasta")

    def close_filehandle(self):
        if self.output_handle is not None:
            self.output_handle.close()


def set_samples(vcf_header, outdir):
    return {sample: VariantSample(sample, outdir) for sample in vcf_header.samples}


def set_common_haplos(samples, chrom, fragment):
    for sample in samples.values():
        sample.extend_haplotypes(chrom, fragment, fragment)


def set_variable_haplos(samples_haplos, chrom, record, ref_frag, var_frag):
    haplos = [ref_frag, var_frag]
    for name, sample in record.samples.items():
        genoA, genoB = sample['GT']
        samples_haplos[name].extend_haplotypes(chrom, haplos[genoA],
                                               haplos[genoB])


def get_chrom_sequence(genome, chrom):
    return str(SeqIO.index(genome, "fasta")[chrom].seq)


def get_chrom_infos_from_vcf(vcf_header, genome):
    chromosomes = defaultdict()
    records = SeqIO.index(genome, "fasta")
    for chrom in list(vcf_header.contigs):
        chromosomes[chrom] = len(records[chrom])
    return chromosomes


def get_chrom_infos_from_fai(reference):
    chromosomes = defaultdict()
    fai_index = reference + ".fai"
    with open(fai_index) as fh:
        for line in fh:
            chrom, length = line.rstrip().split()[0:2]
            chromosomes[chrom] = int(length)
    return chromosomes

def set_reference_genotypes(samples, locus):
    for sample in samples:
        samples[sample].genotypes[locus.id] = [0, 0]


def set_variant_genotypes(samples, locus, record):
    for name, sample in record.samples.items():
        samples[name].genotypes[locus.id] = sample['GT']


def get_variant_record_infos(record):
    """
    Note: Coordinates in pysam are always 0-based (following the python
    convention). pysam documentation (in fact compliant with bam)
    start : 0-based
    end : 1-based (right open)
    We want to be compliant with vcf hence we set everything 1-based
    TODO: should we stay 0-based to follow the python convention ?
    """
    chrom, start, stop, id, svtype = (record.chrom, record.start,
                                      record.stop, record.id,
                                      record.info['SVTYPE'])
    return (chrom, start+1, stop, id, svtype)


def read_variations_from_vcf(vcf_file, genome, outdir):
    """
    Read the vcf files and instentiate all the sample objects
    """
    vcf_in = VariantFile(vcf_file)
    print(vcf_file)
    chromosomes = get_chrom_infos_from_vcf(vcf_in.header, genome)
    samples = set_samples(vcf_in.header, outdir)

    variant_graph = defaultdict()
    for chrom in chromosomes:
        offset = 1
        vg = VariantGraph(chrom)
        for record in vcf_in.fetch(chrom):
            chrom, start, stop, id, svtype = get_variant_record_infos(record)
            common_frag = Fragment(chrom, offset, start - 1)
            unvar_locus = Locus(common_frag)
            set_reference_genotypes(samples, unvar_locus)
            vg.new_locus(unvar_locus)

            if "AF" in record.info:
                allele_freq = record.info['AF']
            else:
                allele_freq = float('nan')
            ref_frag = Fragment(chrom, start, stop)
            var_frag = Variant(chrom, start, stop, id, allele_freq, svtype)
            var_locus = Locus(ref_frag, var_frag)
            set_variant_genotypes(samples, var_locus, record)
            vg.new_locus(var_locus)
            offset = record.stop + 1
        common_frag = Fragment(chrom, offset, chromosomes[chrom])
        unvar_locus = Locus(common_frag)
        set_reference_genotypes(samples, unvar_locus)
        vg.new_locus(unvar_locus)
        variant_graph[chrom] = vg

    for sample in samples.values():
        print(sample)

    population = VariantPopulation(samples.values())
    print(variant_graph)
    return variant_graph, population


def set_chrom_variant_graph(variant_set, chrom, chrom_len):
    offset = 1
    vg = VariantGraph(chrom)
    for sv in sorted(variant_set, key=lambda x: x.start):
        chrom, start, end = sv.chrom, sv.start, sv.end
        common_frag = Fragment(chrom, offset, start - 1)
        unvar_locus = Locus(common_frag)
        vg.new_locus(unvar_locus)

        ref_frag = Fragment(chrom, start, end)
        var_locus = Locus(ref_frag, sv)
        vg.new_locus(var_locus)
        offset = end + 1
    common_frag = Fragment(chrom, offset, chrom_len)
    unvar_locus = Locus(common_frag)
    vg.new_locus(unvar_locus)
    return vg


def reverse(seq):
    """
    Returns a reversed string
    Borrowed from https://gist.github.com/hurshd0
    """
    return seq[::-1]

def complement_base(base):
    complement_dict = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A',
                       'a': 't', 'c': 'g', 'g': 'c', 't': 'a'}

    if base in complement_dict:
        return complement_dict[base]
    else:
        return base

def complement(seq):
    """Returns a complement DNA sequence"""

    seq_list = list(seq)
    # I can think of 3 ways to do this but first is faster I think ???
    # first list comprehension
    seq_list = [complement_base(base) for base in seq_list]
    # second complicated lambda
    # seq_list = list(map(lambda base: complement_dict[base], seq_list))
    # third easy for loop
    # for i in range(len(seq_list)):
    #    seq_list[i] = complement_dict[seq_list[i]]
    return ''.join(seq_list)

def main():
    parser = argparse.ArgumentParser(description="Reformat data for plink",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v', '--vcf', required=True,
                        help='the vcf file')
    parser.add_argument('-r', '--reference', required=True,
                        help='the reference genome')


    args = parser.parse_args()

    read_vcf(args.vcf, args.reference)




if __name__ == '__main__':
    sys.exit(main())
