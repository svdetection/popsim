

#!/usr/bin/env python3

import os
import sys
import re
import random

from lib.exceptions import InputException
from Bio import SeqIO
from joblib import Parallel, delayed
import multiprocessing


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class VariantsSimulator:
    def __init__(self, sv_list: str = None, del_allele_freq: list=(0.2, 0.5), inv_allele_freq: list=(0.2, 0.5), dup_allele_freq: list=(0.2, 0.5), nstretches: str = None,
                 threads: int = -1, stdout=None, stderr=None, silent=False): #MC
        self._epsilon = None
        if sv_list is None:
            sv_list = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "defaults.rules")
        self.min_sizes = {}
        self.variants = self.__load_variants(sv_list)
        self.nstretches = {}
        if nstretches is not None:
            self.nstretches = self.__load_nstretches(nstretches)
        self.deletions = {}
        self.invertions = {}
        self.del_allele_freq = del_allele_freq
        self.inv_allele_freq = inv_allele_freq
        self.dup_allele_freq = dup_allele_freq #MC
        self.threads = threads if threads > -1 else multiprocessing.cpu_count()
        self.stdout = stdout
        self.stderr = stderr
        self.stdout_stash = ""
        self.stderr_stash = ""
        self.silent=silent

    @property
    def epsilon(self):
        if self._epsilon is None:
            self._epsilon = .25
            while 1 + self._epsilon != 1:
                self._epsilon /= 2
        return self._epsilon * 2

    def _print_out(self, message):
        self.__print_to_file(message, self.stdout)

    def _print_err(self, message):
        self.__print_to_file(message, self.stderr)

    def __print_to_file(self, message, log_file, type=0):
        if self.silent:
            return
        if log_file is None:
            print(message, file=sys.stdout if type == 0 else sys.stderr)
        else:
            if type == 0:
                self.stdout_stash += message + "\n"
            else:
                self.stderr_stash += message + "\n"

    def print_flush(self):
        if self.stdout is not None:
            with open(self.stdout, "a") as out:
                out.write(self.stdout_stash)
                self.stdout_stash = ""
        if self.stderr is not None:
            with open(self.stderr, "a") as err:
                err.write(self.stderr_stash)
                self.stderr_stash = ""

    def __print_color(self, color, message, header=None):
        if header is not None:
            header += "\n"
        self._print_out(header + color + message + bcolors.ENDC)

    def print_err(self, message, header=None):
        self.__print_color(bcolors.FAIL, message, header)

    def print_ok(self, message, header=None):
        self.__print_color(bcolors.OKGREEN, message, header)

    def print_warn(self, message, header=None):
        self.__print_color(bcolors.WARNING, message, header)

    def __load_variants(self, sv_list):
        svs = {}
        allowed_sv = {"DEL", "INV", "DUP"} #MC
        with open(sv_list, "r") as sv_list_f:
            for line in sv_list_f:
                if not line.strip():
                    continue
                parts = re.split("\s+", line.strip("\n"))
                type_sv = parts[0]
                if type_sv not in allowed_sv:
                    raise InputException("Disallowed variant type: " + type_sv)
                try:
                    min_s = int(parts[1])
                except ValueError:
                    raise InputException("Error while reading variant: minimal value must be int")
                try:
                    max_s = int(parts[2])
                except ValueError:
                    raise InputException("Error while reading variant: maximal value must be int")
                try:
                    proba = float(parts[3])
                except ValueError:
                    raise InputException("Error while reading variant: proba value must be float")
                if type_sv not in svs:
                    svs[type_sv] = []
                    self.min_sizes[type_sv] = min_s
                svs[type_sv].append({
                    "min": min_s,
                    "max": max_s,
                    "proba": proba
                })
                self.min_sizes[type_sv] = min(self.min_sizes[type_sv], min_s)

        # Only deletions, inversions and duplications are supported for now. #MC
        if "DEL" not in svs and "INV" not in svs and "DUP" not in svs: #MC
            raise Exception("Error: only deletions, inversions and duplications are supported for now!") #MC
        elif (("DEL" not in svs or "INV" not in svs or "DUP" not in svs) and len(svs) > 1) or (len(svs) > 3): #MC
            self.print_err("Warn: only deletions, inversions and duplications are supported for now. Other variant types will be ignored.") #MC

        for type_sv in svs:
            svs[type_sv].sort(key=lambda x: -x["proba"])
            proba_cumul = 0
            for i in range(0, len(svs[type_sv])):
                proba_cumul += svs[type_sv][i]["proba"]
                svs[type_sv][i]["proba_cumul"] = round(proba_cumul, 10)
            if not 1-self.epsilon <= proba_cumul <= 1+self.epsilon:
                raise InputException("{0}: sum of probabilities does not equals 1!".format(type_sv))

        return svs

    @staticmethod
    def __load_nstretches(ns_file):
        stretches = {}
        with open(ns_file, 'r') as stretches_f:
            for stretch in stretches_f:
                parts = stretch.strip("\n").split("\t")
                chrm = parts[0]
                start = int(parts[1])
                end = int(parts[2])
                length = int(parts[4])
                if chrm not in stretches:
                    stretches[chrm] = []
                stretches[chrm].append([start, end, length])
        for chrm in stretches:
            stretches[chrm].sort(key=lambda x: x[0])
        return stretches

    def _get_inversion_size(self):
        r = random.uniform(0, 1)
        for sizes in self.variants["INV"]:
            if r < sizes["proba_cumul"]:
                break
        return random.randint(sizes["min"], sizes["max"] + 1)

    def _get_deletion_size(self):
        r = random.uniform(0, 1)
        for sizes in self.variants["DEL"]:
            if r < sizes["proba_cumul"]:
                break
        return random.randint(sizes["min"], sizes["max"] + 1)

    def _get_duplication_size_and_shift(self): #<MC>
        r = random.uniform(0, 1)
        for sizes in self.variants["DUP"]:
            if r < sizes["proba_cumul"]:
                break
        return random.randint(sizes["min"], sizes["max"] + 1), 0 #</MC>

    def __get_max_interval(self, start, end, chrm, pos):
        my_stretches = []
        for c_nstretches in self.nstretches[chrm][pos:]:
            if (start - 50 < c_nstretches[1] <= end + 50) or (start - 50 <= c_nstretches[0] < end + 50):
                my_stretches.append([c_nstretches[0] - 50, c_nstretches[1] + 50])
            else:
                break
        intervals = []
        my_start = start
        for my_stretch in my_stretches:
            if my_stretch[0] > my_start:
                intervals.append([my_start, my_stretch[0]])
            my_start = min(my_stretch[1], end)
        if my_start < end:
            intervals.append([my_start, end])
        if len(intervals) == 0:  # No valid interval found
            return 0, 0
        intervals.sort(key=lambda x: x[0]-x[1])
        return intervals[0]

    def _get_far_from_nstretches(self, chrm, start, end, chr_len, pos=0):
        """
        Get if a deletion is far from N stretches
        :param chrm: the chromosome
        :param start: start position of the deletion
        :param end: end position of the deletion
        :param pos: position to start in stretches list
        :return: three values:
            - True/False: True if far from nstretches (after a resize if needed)
            - True/False: True if deletion size has changed
            - True/False: True if deletion position changed (at least start)
            - Int: new start of the deletion
            - Int: new end of the deletion
            - Pos: pos to restart (-1 if no need to restart)
        """
        if chrm in self.nstretches:
            for c_nstretches in self.nstretches[chrm][pos:]:
                pos += 1
                if c_nstretches[0] <= start < c_nstretches[1]:
                    # --------(n_start)------(start)----------(n_end)---------(end)-----
                    # or
                    # --------(n_start)------(start)----------(end)---------(n_end)-----
                    if end > c_nstretches[1] + 50:
                        # --------(n_start)------(start)----------(n_end)----{>50bp}----(end)-----
                        new_start = c_nstretches[1] + 50
                        length = end - new_start
                        if length >= self.min_sizes["DEL"]:
                            return True, True, True, new_start, end, pos
                    # Else, deletion is too much close to the nstretch (or included inside), and/or can't be resized
                    return False, False, False, start, end, -1

                elif start < c_nstretches[0] < end and start < c_nstretches[1] < end:
                    # ------------(start)--------(n_start)--------(n_end)-------(end)---
                    # Find best interval:
                    new_start, new_end = self.__get_max_interval(start, end, chrm, pos-1)
                    length = new_end - new_start
                    if length > 0 and length >= self.min_sizes["DEL"]:
                        return True, True, True, new_start, new_end, -1
                    # Else, deletion can't be rezised
                    return False, False, False, start, end, -1

                elif start < c_nstretches[0] < end or c_nstretches[0] > end > c_nstretches[0] - 50:
                    # FIRST CASE:
                    # ------------(start)--------(n_start)--------(end)-------(n_end)---
                    # SECOND CASE:
                    # ------------(start)--------(end)---{<50bp}---(n_start)------------
                    new_end = c_nstretches[0] - 50
                    length = new_end - start
                    if length > 0 and length >= self.min_sizes["DEL"]:
                        return True, True, True, start, new_end, pos
                    # Else, deletion can't be rezised
                    return False, False, False, start, end, -1

                elif c_nstretches[1] < start < c_nstretches[1] + 50:
                    # ------------(n_end)---{<50bp}---(start)-------------------(end)---
                    new_start = c_nstretches[1] + 50
                    new_end = end + (new_start - start)
                    resize = False
                    move = True
                    if new_end > chr_len:
                        move = False
                        resize = True
                        new_end = chr_len
                        length = new_end - new_start
                        if length < 0 or length < self.min_sizes["DEL"]:
                            # Unable to move / resize
                            return False, False, False, start, end, -1
                    return True, resize, move, new_start, new_end, pos

                elif c_nstretches[0] > end + 50:
                    break
        return True, False, False, start, end, -1

    def _get_random_variants_by_chr(self, chrm, chr_length, deletions,
                                    inversions, duplications, proba_deletion,
                                    proba_inversion, proba_duplication):  # MC
        self._print_out("Generating for chromosome %s..." % chrm)
        nb_dels_on_ch = 0
        nb_invs_on_ch = 0
        nb_dups_on_ch = 0  # MC
        nb_inv_on_ch = 0
        nb_inv = 0
        i = 0
        len_chr = chr_length[chrm]
        deletions[chrm] = []
        inversions[chrm] = []
        duplications[chrm] = []  # MC
        proba_inversion_cum = proba_deletion + proba_inversion
        proba_duplication_cum = proba_deletion + proba_inversion + proba_duplication
        duplication_insertion_sites = []

        while i < len_chr:
            r = random.uniform(0, 1)
            if r <= proba_deletion:
                nb_dels_on_ch += 1
                del_size = self._get_deletion_size()
                start = i + 1
                end = i + 1 + del_size
                if end > len_chr:  # MC
                    end = len_chr  # MC
                    del_size = end - start  # MC
                print_message_header = "One deletion found ({0}:{1}-{2} ; size: {3})...".format(chrm, start, end,
                                                                                                del_size)
                pos = 0
                keep_deletion = True
                has_resize = False
                has_move = False
                while keep_deletion and pos >= 0:
                    keep_deletion, with_resize, with_move, start, end, pos = self._get_far_from_nstretches(chrm, start,
                                                                                                           end, len_chr,
                                                                                                           pos)
                    if with_resize:
                        del_size = end - start
                        has_resize = True
                    elif with_move:
                        has_move = True

                if keep_deletion:
                    if has_resize:
                        self.print_warn("N-stretch filter: resized ({0}:{1}-{2}, new size:{3})".
                                        format(chrm, start, end, del_size), print_message_header)
                    elif has_move:
                        self.print_warn("N-stretch filter: moved ({0}:{1}-{2})".format(chrm, start, end),
                                        print_message_header)
                    else:
                        self.print_ok("N-stretch filter: OK", print_message_header)
                else:
                    self.print_err("N-stretch filter: removed", print_message_header)

                if keep_deletion:
                    for pos in [start,end]:
                        if pos in duplication_insertion_sites :
                            keep_deletion = False
                            self.print_err(self.print_err("deletion {0}:{1}-{2} covers a duplication target site: deletion removed".format(chrm, start, end), print_message_header), print_message_header)
                if keep_deletion:
                    i = start
                    freq = random.choice(self.del_allele_freq)
                    deletions[chrm].append({
                        "name": "DEL{0}_{1}".format(chrm, nb_dels_on_ch),
                        "start": start,
                        "end": end,
                        "length": del_size,
                        "allele_freq": freq
                    })
                    i += del_size

            elif r <= proba_inversion_cum:
                nb_inv += 1
                nb_inv_on_ch += 1
                inv_size = self._get_inversion_size()
                start = i + 1
                end = i + 1 + inv_size
                if end > len_chr: #MC
                    end = len_chr #MC
                    inv_size = end - start #MC
                print_message_header = "One inversion found ({0}:{1}-{2} ; size: {3})...".format(chrm, start, end,
                                                                                                 inv_size)
                pos = 0
                keep_inversion = True
                has_resize = False
                has_move = False
                while keep_inversion and pos >= 0:
                    keep_inversion, with_resize, with_move, start, end, pos = self._get_far_from_nstretches(chrm, start,
                                                                                                           end, len_chr,
                                                                                                           pos)
                    if with_resize:
                        inv_size = end - start
                        has_resize = True
                    elif with_move:
                        has_move = True
                if keep_inversion:
                    if has_resize:
                        self.print_warn("N-stretch filter: resized ({0}:{1}-{2}, new size:{3})".
                                        format(chrm, start, end, inv_size), print_message_header)
                    elif has_move:
                        self.print_warn("N-stretch filter: moved ({0}:{1}-{2})".format(chrm, start, end),
                                        print_message_header)
                    else:
                        self.print_ok("N-stretch filter: OK", print_message_header)
                else:
                        self.print_err("N-stretch filter: removed", print_message_header)

                if keep_inversion:
                    for pos in [start,end]:
                        if pos in duplication_insertion_sites :
                            keep_inversion = False
                            self.print_err("inversion {0}:{1}-{2} covers a duplication target site: inversion removed".format(chrm, start, end), print_message_header)

                if keep_inversion:
                    i = start
                    freq = random.choice(self.inv_allele_freq)
                    inversions[chrm].append({
                        "name": "INV{0}_{1}".format(chrm, nb_inv_on_ch),
                        "start": start,
                        "end": end,
                        "length": inv_size,
                        "allele_freq": freq
                    })
                    i += inv_size
            elif r <= proba_duplication_cum:  # <MC>
                nb_dups_on_ch += 1
                dup_size, dup_shift = self._get_duplication_size_and_shift()
                start = i + 1
                end = i + 1 + dup_size
                if end > len_chr:  # MC
                    end = len_chr  # MC
                    dup_size = end - start  # MC

                print_message_header = "One duplication found ({0}:{1}-{2} ({3}); size: {4})...".format(chrm, start, end, dup_shift, dup_size)
                pos = 0
                keep_duplication = True
                has_resize = False
                has_move = False
                bad_shift = False
                while keep_duplication and pos >= 0:
                    keep_duplication, with_resize, with_move, start, end, pos = self._get_far_from_nstretches(chrm, start,
                                                                                                           end, len_chr,
                                                                                                           pos)
                    if with_resize:
                        dup_size = end - start
                        has_resize = True
                    elif with_move:
                        has_move = True


                if keep_duplication:
                    if has_resize:
                        self.print_warn("N-stretch filter: resized ({0}:{1}-{2}, new size:{3})".
                                        format(chrm, start, end, dup_size), print_message_header)
                    elif has_move:
                        self.print_warn("N-stretch filter: moved ({0}:{1}-{2})".format(chrm, start, end),
                                        print_message_header)
                    else:
                        self.print_ok("N-stretch filter: OK", print_message_header)
                else:
                    self.print_err("N-stretch filter: removed", print_message_header)

                if keep_duplication:
                    for pos in [start,end]:
                        if pos in duplication_insertion_sites :
                            keep_duplication = False
                            self.print_err("duplication {0}:{1}-{2} (3) covers another duplication target site: duplication removed".format(chrm, start, end, dup_shift), print_message_header)
                    if end + dup_shift > len_chr :
                            keep_duplication = False
                            self.print_err("duplication {0}:{1}-{2} (3) : duplication target site outside the original seq: duplication removed".format(chrm, start, end, dup_shift), print_message_header)


                if chrm in self.nstretches:
                    for n in self.nstretches[chrm]:
                        if end + dup_shift >= n[0] and end + dup_shift <= n[1]:
                                keep_duplication = False
                                self.print_err("duplication {0}:{1}-{2} (3) : duplication target site in Nstretch".format(chrm, start, end, dup_shift), print_message_header)
                                break

                if keep_duplication:
                    duplication_insertion_sites.append(end + dup_shift)
                    i = start
                    freq = random.choice(self.dup_allele_freq)
                    duplications[chrm].append({
                        "name": "DUP{0}_{1}".format(chrm, nb_dups_on_ch),
                        "start": start,
                        "end": end,
                        "length": dup_size,
                        "allele_freq": freq,
                        "shift":dup_shift
                    })
                    i += dup_size #</MC>
            i += 1

        # for u in duplications:
        #     print("\\nnDUP\n",duplications)
        #
        # for d in deletions:
        #     print("\\nnDEL\n",deletions)
        #
        # for i in inversions:
        #     print("\\nnINV\n",inversions)

        #sys.exit(0)
        return len(deletions[chrm]), deletions, len(inversions[chrm]), inversions, len(duplications[chrm]), duplications, self.stdout_stash, self.stderr_stash

    def get_random_variants(self, proba_deletion, proba_inversion, proba_duplication, reference_genome): #MC
        fasta_index = SeqIO.index(reference_genome, "fasta")
        deletions = {}
        inversions = {}
        duplications = {} #MC
        chr_length = {}
        for chrm in fasta_index:
            chr_length[str(chrm)] = len(fasta_index[chrm].seq)
            #if chrm not in self.nstretches.keys():
            #   self.nstretches[chrm] = {}

        results = Parallel(n_jobs=self.threads)(delayed(self._get_random_variants_by_chr)
                                                (str(chrm), chr_length, deletions, inversions, duplications, proba_deletion, proba_inversion,proba_duplication) #MC
                                                for chrm in fasta_index)
        nb_dels = 0
        nb_invs = 0
        nb_dups = 0 #MC

        for result in results:
            nb_dels += result[0]
            deletions.update(result[1])
            nb_invs += result[2]
            inversions.update(result[3])
            nb_dups += result[4] #MC
            duplications.update(result[5]) #MC
            self.stdout_stash += result[6] #MC
            self.stderr_stash += result[7] #MC
        self.deletions = deletions
        self.inversions = inversions
        self.duplications = duplications
        return nb_dels, deletions, nb_invs, inversions, nb_dups, duplications #MC

    def print_variants(self):
        self._print_out("SV\tCHR\tSTART\tEND\tLENGTH\tFREQ")
        for chrm, deletes in self.deletions.items():
            for delete in deletes:
                self._print_out("{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(delete["name"], chrm, delete["start"], delete["end"],
                                                            delete["length"], delete["allele_freq"]))


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Generate random variants positions on a genome')
    parser.add_argument("--sv-list", help="Variants simulation description file")
    parser.add_argument("--proba-del", help="Probabilty to have a deletion", type=float, default=0.000001)
    parser.add_argument("--reference", help="Reference genome", required=True)
    args = parser.parse_args()
    try:
        simultator = VariantsSimulator(args.sv_list)
        simultator.get_random_variants(args.proba_del, args.reference)
        simultator.print_variants()
    except InputException as e:
        print(e)


if __name__ == '__main__':
    sys.exit(main())
