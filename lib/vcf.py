import os
import sys

prg_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, os.path.join(prg_path, "svlib"))

from svreader.vcfwrapper import VCFReader


ALLOW_VARIANTS = ['DEL', 'INV']


def passed_variant(record):
    """
    Did this variant pass?
    :param record: vcf record object
    :return: True if pass, False else
    """
    return record.filter is None or len(record.filter) == 0 or "PASS" in record.filter


def readvariants(vcffile: str, type_v:str, variants: dict=None, dofilter: bool=False, all_variants: dict=None):
    """
    Read variants from a VCF file
    :param vcffile: input vcf file
    :param type_v: type of variant to get
    :param variants: dict containing variants (only filtered if dofilter)
    :param dofilter: filter to get only PASS variants
    :param all_variants: all variants (ignored if is None or if dofilter is False)
    :return:
    """
    if variants is None:
        variants = {}
    if type_v.upper() not in ALLOW_VARIANTS:
        raise ValueError("Invalid variant type: %s" % type_v)

    vcfin = VCFReader(vcffile)
    for r in vcfin:
        if r.sv_type.upper() == type_v:
            if not dofilter or passed_variant(r):
                variants[r.id] = r
            if dofilter and all_variants is not None:
                all_variants[r.id] = r
    if dofilter and all_variants is not None:
        return variants, all_variants
    return variants
