#!/usr/bin/env python3

import sys
import os
import shutil
import random
import argparse
import configparser
import re
import traceback
import multiprocessing
import subprocess
from collections import OrderedDict, defaultdict
import vcf
from pysam import VariantFile, FastaFile
from Bio.Seq import Seq
import tempfile
import time
from pysam import tabix_compress, tabix_index, VariantFile
from lib.variants_simulator import VariantsSimulator
from lib.exceptions import InputException, ExecException


from random import random

from lib.Variants import Variant, VariantSample, VariantPopulation
from lib.Variants import set_chrom_variant_graph, get_chrom_sequence
from lib.Variants import get_chrom_infos_from_fai, read_variations_from_vcf


class Singleton(type):
    """
    Implementation of a Singleton class
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Print(metaclass=Singleton):
    """
    A Printer singleton class
    """
    def __init__(self, stdout=None, stderr=None, silent=False):
        self.stdout = stdout
        self.stderr = stderr
        self.silent = silent
        self.start_time = time.time()
        if self.stdout is not None and os.path.isfile(self.stdout):
            os.remove(self.stdout)
        if self.stderr is not None and os.path.isfile(self.stderr):
            os.remove(self.stderr)

    def elapsed_time(self):
        return "%5.2f" %(time.time() - self.start_time)

    def out(self, message):
        if self.silent:
            return
        if self.stdout is not None:
            with open(self.stdout, "a") as stdout_f:
                print(message, file=stdout_f)
        else:
            print(message, file=sys.stdout)

    def err(self, message):
        if self.silent:
            return
        if self.stderr is not None:
            with open(self.stderr, "a") as stderr_f:
                print(message, file=stderr_f)
        else:
            print(message, file=sys.stderr)


def get_source_dir():
    prg_path = os.path.dirname(os.path.realpath(__file__))
    return prg_path


def check_min_size(value):
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("%s is an invalid size value (>= 0)" % value)
    return ivalue


def isboolstr(v):
    if v.lower() in ('yes', 'true',
                     'no', 'false'):
        return True
    else :
        return False


def boolstr2bool(v):
    if v.lower() in ('yes', 'true'):
        return True
    elif v.lower() in ('no', 'false'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_args(argv=None):
    """
    Parse script arguments
    :return: argparse arguments object
    """
    prg_path = os.path.dirname(os.path.realpath(__file__))

    # We try here to use a config file with the following behavior:
    # Start by using parse_known_args() to parse a configuration file
    # from the commandline, then read it with ConfigParser and set the
    # defaults, and then parse the rest of the options with parse_args().
    # This will allow you to have a default value, override that with a
    # configuration file and then override that with a commandline option
    # see https://stackoverflow.com/questions/3609852/which-is-the-best-way-to-allow-configuration-options-be-overridden-at-the-comman

    if argv is None:
        argv = sys.argv

    # Parse any conf_file specification
    # We make this parser with add_help=False so that
    # it doesn't parse -h and print help.
    conf_parser = argparse.ArgumentParser(
        description=__doc__,  # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
        )
    conf_parser.add_argument("--conf-file",
                             help="Specify config file", metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()

    defaults = {'sv_list': prg_path + "/event_frequency.svlist",
                'nb_inds': 5,
                'coverage': 15,
                'output_dir': "res",
                'force_outputdir': False,
                'haploid': False,
                'force_polymorphism': False,
                'no-reads': False,
                'proba_del': 0.000001,
                'proba_dup': 0.000001,
                'proba_inv': 0.000001,
                'del_allele_freq': [0.2, 0.5],
                'dup_allele_freq': [0.2, 0.5],
                'inv_allele_freq': [0.2, 0.5],
                'read_len': 100,
                'insert_len_mean': 300,
                'insert_len_sd': 30,
                'quiet': False,
                'min_deletions': 0,
                'min_duplications': 0,
                'min_inversions': 0,
                'max_try': 10,
                'threads': 1  # multiprocessing.cpu_count()
                }

    if args.conf_file:
        config = configparser.SafeConfigParser()
        config.read([args.conf_file])
        # PATCH in order to convert a list-string to a list of floats
        p = re.compile(r"\[(?P<freqs>[0-9\,\.\s]+)\]")
        defaults_dict = {}
        for k, v in config.items("Defaults"):
            if "freq" in k:
                m = p.match(v).group('freqs')
                values = [float(i) for i in m.split(",")]
                defaults_dict[k] = values
            elif isboolstr(v):
                defaults_dict[k] = boolstr2bool(v)
            else:
                defaults_dict[k] = v
        defaults.update(defaults_dict)
        # defaults.update(dict(config.items("Defaults")))

    # Parse rest of arguments
    # Don't suppress add_help here so it will handle -h
    parser = argparse.ArgumentParser(
        description='Generate simulated populations with SV',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        # Inherit options from config_parser
        parents=[conf_parser]
        )

    parser.add_argument("-n", "--nb-inds",
                        help="Number of individuals to generate", type=int)
    parser.add_argument("-r", "--reference",
                        help="Reference genome", required=True)
    parser.add_argument("-ns", "--nstretches",
                        help="N-stretches positions bed file", required=False)
    parser.add_argument("-s", "--sv-list",
                        help="File containing the SVs")
    parser.add_argument("-c", "--coverage", help="Coverage of reads", type=int)
    parser.add_argument("-o", "--output-dir", help="Output directory")
    parser.add_argument("-e", "--force-outputdir",
                        help="Delete output directory before start, "
                             "if already exists",
                        action='store_const', const=True)
    parser.add_argument("-f", "--force-polymorphism",
                        help="Force polymorphism for each SV",
                        action='store_const', const=True)
    parser.add_argument("-a", "--haploid",
                        help="Make a haploid genome, instead of diploid one",
                        action="store_const", const=True)
    parser.add_argument("--no-reads",
                        help="Only simulate rearranged genomes, do not simulate reads",
                        action="store_const", const=True)
    parser.add_argument("-pd", "--proba-del",
                        help="Probabilty to have a deletion", type=float)
    parser.add_argument("-pu", "--proba-dup",
                        help="Probabilty to have a duplication", type=float)
    parser.add_argument("-pi", "--proba-inv",
                        help="Probablity to have an insertion", type=float)
    parser.add_argument('-fd', '--freq-del', type=float, required=False,
                        help="Frequencies choices for deletions, space separated",
                        nargs="+")
    parser.add_argument('-fi', '--freq-inv', type=float, required=False,
                        help="Frequencies choices for inversions, space separated",
                        nargs="+")
    parser.add_argument('-fu', '--freq-dup', type=float, required=False,
                        help="Frequencies choices for duplications, space separated",
                        nargs="+") #MC
    parser.add_argument("-l", "--read-len",
                        help="Generate reads having a length of LEN", type=int)
    parser.add_argument("-m", "--insert-len-mean",
                        help="Generate inserts (fragments) having an average length of LEN",
                        type=int)
    parser.add_argument("-v", "--insert-len-sd",
                        help="Set the standard deviation of the insert (fragment) length (%%)",
                        type=int)
    parser.add_argument("-q", "--quiet",
                        help="Don't ask anything, choose default answer instead",
                        action="store_const", const=True)
    parser.add_argument("--silent",
                        help="No standard output",
                        action="store_const", const=True)
    parser.add_argument("-md", "--min-deletions",
                        help="Minimum of deletions to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("-mi", "--min-inversions",
                        help="Minimum of inversions to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("-mu", "--min-duplications",
                        help="Minimum of duplications to generate (>=1)",
                        type=check_min_size)
    parser.add_argument("-mt", "--max-try", help="Maximum of tries",
                        type=int)
    parser.add_argument("-t", "--threads",
                        help="Number of threads", type=int)
    parser.add_argument("-g", "--genotypes",
                        help="Position of SV with genotypes of individuals")
    #print(defaults)
    parser.set_defaults(**defaults)


    args = parser.parse_args(remaining_argv)

    if args.nb_inds is None and args.genotypes is None:
        parser.error("--nb-inds parameter is required (except if --genotypes parameter is given)")

    if args.proba_del > 1 or args.proba_del < 0:
        parser.error("Probability of deletions must be between 0 and 1")

    if args.proba_inv > 1 or args.proba_inv < 0:
        parser.error("Probability of inversions must be between 0 and 1")

    if args.proba_dup > 1 or args.proba_dup < 0:
        parser.error("Probability of duplication must be between 0 and 1")

    if args.proba_del + args.proba_inv + args.proba_dup > 1:
        parser.error("Sum of inversions/deletions/duplications probabilities must be lesser than 1")

    return args

def confirm(deletions: dict, inversions: dict, duplications: dict, variants: dict,
             ask=True):
    """
      Verbose description of the variants created
    """
    printer = Print()
    # Deletions:
    nb_dels = 0
    if "DEL" in variants:
        variants_del = sorted(variants["DEL"], key=lambda x: x["min"])
        variants_del_counts = OrderedDict()
        variants_del_counts_chr = {}
        for variant in variants_del:
            variants_del_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, deletes in deletions.items():
            nb_dels_chr = len(deletes)
            nb_dels += nb_dels_chr
            variants_del_counts_chr[chrm] = nb_dels_chr
            for delete in deletes:
                v_len = delete["length"]
                for variant in variants_del:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_del_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("{0} deletion variants have been generated.".format(nb_dels))
        if nb_dels > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_del_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_del_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_del_counts_chr[chrm]))
        printer.out("")

    # Inversions:
    nb_invs = 0
    if "INV" in variants:
        variants_inv = sorted(variants["INV"], key=lambda x: x["min"])
        variants_inv_counts = OrderedDict()
        variants_inv_counts_chr = {}
        for variant in variants_inv:
            variants_inv_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, the_inversions in inversions.items():
            nb_invs_chr = len(the_inversions)
            nb_invs += nb_invs_chr
            variants_inv_counts_chr[chrm] = nb_invs_chr
            for inversion in the_inversions:
                v_len = inversion["length"]
                for variant in variants_inv:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_inv_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("{0} inversion variants have been generated.".format(nb_invs))
        if nb_invs > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_inv_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_inv_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_inv_counts_chr[chrm]))
        printer.out("")

    # Duplications: #<MC>
    nb_dups = 0
    if "DUP" in variants:
        variants_dup = sorted(variants["DUP"], key=lambda x: x["min"])
        variants_dup_counts = OrderedDict()
        variants_dup_counts_chr = {}
        for variant in variants_dup:
            variants_dup_counts["{0}-{1}".format(variant["min"], variant["max"])] = 0
        for chrm, the_duplications in duplications.items():
            nb_dups_chr = len(the_duplications)
            nb_dups += nb_dups_chr
            variants_dup_counts_chr[chrm] = nb_dups_chr
            for duplication in the_duplications:
                v_len = duplication["length"]
                for variant in variants_dup:
                    if variant["min"] < v_len <= variant["max"]:
                        variants_dup_counts["{0}-{1}".format(variant["min"], variant["max"])] += 1
                        break
        printer.out("{0} duplication variants have been generated.".format(nb_dups))
        if nb_dups > 0:
            printer.out("Ranges:")
            for v_range, v_count in variants_dup_counts.items():
                printer.out("\t- Range {0}: {1}".format(v_range, v_count))
            printer.out("Chromosomes:")
            for chrm in sorted(list(variants_dup_counts_chr.keys())):
                printer.out("\t- {0}: {1}".format(chrm, variants_dup_counts_chr[chrm]))
        printer.out("") #</MC>

    # Confirm:
    if ask:
        return input("Continue [Y/n]? ") in ["y", "Y", ""]
    return True


def VariantSimulationLog(type, minimum, max_del, max_inv, max_dup):
    printer = Print()
    printer.err("\nUnable to generate %s %ss.\nTry to reduce size or increase "
                         "general probability to have a %s." % (minimum, type, type))
    printer.out("\nMaximum of each event: %d deletions, %d inversions, %d duplications." % (max_del, max_inv, max_dup)) #MC


def generate_variant_graph(sv_list, nstretches, nb_inds, genotypes,
                           variant_params, reference,
                           output_dir, output_vcf, threads, max_try,
                           stdout, stderr,
                           quiet, silent):
    """
    Generate the variant graph:
       - from a vcf files
       - from scratch, simulatin random variants as well as randome genotypes,
         write the genotypes to a vcf file
    """
    printer = Print()
    if genotypes is not None:
        vg, vp = read_variations_from_vcf(genotypes, reference, output_dir)
    else:
        vg = simulate_variations(sv_list, nstretches,
                                 variant_params, reference,
                                 output_dir,
                                 max_try=max_try, threads=threads,
                                 stdout=stdout, stderr=stderr,
                                 quiet=quiet, silent=silent)

        vp = generate_variant_population(vg, nb_inds, output_dir)
        write_vcf_tofile(vg, vp, output_vcf, reference)

    return vg, vp


def simulate_variations(sv_list, nstretches, var_params,
                        reference, output_dir,
                        max_try, threads,
                        stdout, stderr,
                        quiet, silent):
    """
    The core function:
        - for the given del, inv and dup probabiliies, simulate variant


    """
    printer = Print()

    proba_del, proba_inv, proba_dup = (var_params['proba_del'],
                                       var_params['proba_inv'],
                                       var_params['proba_dup'])

    min_del, min_inv, min_dup = (var_params['min_del'],
                                 var_params['min_inv'],
                                 var_params['min_dup'])

    del_allele_freq, inv_allele_freq, dup_allele_freq = (var_params['del_allele_freq'],
                                                         var_params['inv_allele_freq'],
                                                         var_params['dup_allele_freq'])

    elapsed_time = printer.elapsed_time()
    printer.out("Generating random variations...%s seconds\n" % elapsed_time)
    try:
        nb_del = -1  # To allow 0 deletions
        nb_inv = -1  # To allow 0 inversions
        nb_dup = -1  # To allow 0 inversions #MC
        nb_try = 0
        sv_sim = VariantsSimulator(sv_list=sv_list, nstretches=nstretches,
                                   del_allele_freq=del_allele_freq,
                                   inv_allele_freq=inv_allele_freq,
                                   dup_allele_freq=dup_allele_freq,
                                   stdout=stdout, stderr=stderr,
                                   threads=threads, silent=silent)  # MC

        proba_del = proba_del if "DEL" in sv_sim.variants else 0
        proba_inv = proba_inv if "INV" in sv_sim.variants else 0
        proba_dup = proba_dup if "DUP" in sv_sim.variants else 0

        printer.out("Try to generate at least %s deletions and %s inversions..." % (min_del, min_inv))
        max_del = 0
        max_inv = 0
        max_dup = 0 #MC

        while ((nb_del < min_del or nb_inv < min_inv or nb_dup < min_dup)
               and nb_try < max_try): #MC
            printer.out("\nTry {0} / {1}...".format(nb_try + 1, max_try))
            nb_del, deletions, nb_inv, inversions, nb_dup, duplications = sv_sim.get_random_variants(proba_del, proba_inv, proba_dup, reference)  # MC
            max_del = max(max_del, nb_del)
            max_inv = max(max_inv, nb_inv)
            max_dup = max(max_dup, nb_dup)  # MC
            sv_sim.print_flush()
            nb_try += 1
        if nb_del < min_del:
            VariantSimulationLog("deletion", min_del, max_del, max_inv, max_dup)
            exit(1)
        if nb_inv < min_inv:
            VariantSimulationLog("inversion", min_inv, max_del, max_inv, max_dup)
            exit(1)
        if nb_dup < min_dup:
            VariantSimulationLog("duplication", min_dup, max_del, max_inv, max_dup)
            exit(1)
        printer.out("")
    except InputException as e:
        printer.err(e)
        return 1
    except IOError as e:
        printer.err(e)
        return 1
    except Exception:
        printer.err(traceback.format_exc())
        return 1

    # TODO join deletions, inversions and duplications before confirm
    ok = True
    if quiet:
        ok = confirm(deletions, inversions, duplications, sv_sim.variants, False) #MC
    else:
        ok = confirm(deletions, inversions, duplications, sv_sim.variants) #MC

    # TODO : do something with ok
    vg = set_variant_graph(sv_sim, reference)
    return vg


def set_variant_graph(sv_sim, reference):
    variant_set = defaultdict(list)
    for chrom, variants in sv_sim.deletions.items():
        for variant in variants:
            sv = Variant(chrom, variant['start'], variant['end'],
                         variant['name'], variant['allele_freq'], "DEL")
            variant_set[chrom].append(sv)
    for chrom, variants in sv_sim.inversions.items():
        for variant in variants:
            sv = Variant(chrom, variant['start'], variant['end'],
                         variant['name'], variant['allele_freq'], "INV")
            variant_set[chrom].append(sv)
    for chrom, variants in sv_sim.duplications.items():
        for variant in variants:
            sv = Variant(chrom, variant['start'], variant['end'],
                         variant['name'], variant['allele_freq'], "DUP")
            variant_set[chrom].append(sv)

    # split by chromosomes
    fa = FastaFile(reference)
    vg = defaultdict()
    for chrom in variant_set:
        chrom_len = fa.get_reference_length(chrom)
        vg[chrom] = set_chrom_variant_graph(variant_set[chrom], chrom, chrom_len)

    return vg


def generate_variant_population(variant_graph, nb_inds, output_dir):
    printer = Print()
    elapsed_time = printer.elapsed_time()
    printer.out("Generating random genotypes...%s seconds\n" % elapsed_time)
    population = []
    for i in range(1, nb_inds+1):
        ind = "indiv%d" % i
        population.append(VariantSample(ind, output_dir))
    variant_pop = VariantPopulation(population)

    for chrom in variant_graph:
        for locus in variant_graph[chrom].traversal():
            set_random_genotypes(locus, variant_pop)
    return variant_pop


def get_population_genotypes(variant_population, locus):
    vcf_genotypes = []
    for sample in variant_population.ordered_samples():
        geno = sample.get_genotype_str(locus)
        vcf_geno = vcf.model._Call(None, sample.ident,
                                   vcf.model.make_calldata_tuple("GT")(GT=geno))
        vcf_genotypes.append(vcf_geno)
    return vcf_genotypes


def print_genotypes(variant_graph, variant_population):
    for chrom in variant_graph:
        for locus in variant_graph[chrom].traversal():
            geno_str = []
            for sample in variant_population.ordered_samples():
                geno_str.append("%d/%d" % (sample.genotypes[locus.id][0],
                                           sample.genotypes[locus.id][1]))
            print("%s\t%3.2f\t%s" % (locus, locus.variant_proba,
                                     "\t".join(geno_str)))


def set_random_genotypes(locus, population):
    """
    For each common locus all individuals are homozygote ref
    For variant locus we select genotype at random according to the population
    allele freq
    """
    num_alt = 0
    for sample in population.ordered_samples():
        if locus.is_common:  # always homozygote reference
            sample.genotypes[locus.id] = [0, 0]
            continue
        for haplotype in [0, 1]:
            # generate random number [0,1), alt genotype is chosen accordingly
            if random() <= locus.variant_proba:
                num_alt += 1
                sample.genotypes[locus.id].append(1)
            else:
                sample.genotypes[locus.id].append(0)
    if num_alt == 0:
        #we select at random a chromosome and set it to 1
        v = 1


def dump_fastas_tofile(variant_graph, variant_population, reference):
    """
    For each chrom, store the chromosome sequence in memory and dump for each
    individual the two haplotypes
    """
    printer = Print()
    elapsed_time = printer.elapsed_time()
    printer.out("Generating fasta chromosome haplotypes..."
                "%s seconds\n" % elapsed_time)
    for chrom in variant_graph:
        chrom_seq = get_chrom_sequence(reference, chrom)
        for sample in variant_population.ordered_samples():
            elapsed_time = printer.elapsed_time()
            printer.out("chromosomes %s for %s...%s seconds" % (chrom, sample.ident,
                                                                elapsed_time))
            sample.dump_chrom_fastas(chrom, variant_graph[chrom], chrom_seq)
    variant_population.close_filehandles()


def write_vcf_tofile(variant_graph, variant_population, output_vcf, reference):
    """
    Ignoring segments homozygote for reference store the genotype variant sample
    information in vcf records and dump it to a file
    """
    records = []
    for chrom in variant_graph:
        for locus in variant_graph[chrom].traversal():
            if locus.is_common:
                continue
            alt = locus.alternate
            chrom, start, end, id, svtype = alt.get_svinfos()
            genotypes = get_population_genotypes(variant_population, locus)
            info = {"END": end, "AF": alt.allele_freq,
                    "SVTYPE": svtype, "SVLEN": alt.get_len()}
            vcf_record = vcf.model._Record(chrom, start, id,
                                           "N", [vcf.model._SV(svtype)],
                                           ".", "PASS", info, "GT", [0], genotypes)
            records.append(vcf_record)
    ordered_samples = [s.ident for s in variant_population.ordered_samples()]
    dump_to_vcf_file(output_vcf, records, ordered_samples, reference)


def dump_to_vcf_file(output_vcf, records, ordered_samples, reference):
    """
    Simple wrapper to dump to output_vcf
    """
    vcf_header_str = _build_vcf_header(ordered_samples, reference)
    f = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
    f.write(vcf_header_str)
    f.close()

    with open(output_vcf, "w") as o_vcf:
        vcf_reader = vcf.Reader(filename=f.name)
        vcf_writer = vcf.Writer(o_vcf, vcf_reader)
        for record in records:
            vcf_writer.write_record(record)
        vcf_writer.close()

    tabix_compress(output_vcf, output_vcf + ".gz", True)
    tabix_index(output_vcf + ".gz", force=True, preset="vcf")
    os.remove(f.name)
    os.remove(output_vcf)


def _build_vcf_header(ordered_samples, reference):
    """
    Build header of the VCF file
    :param variant_populations: number of individuals {int}
    """
    vcf_header_str = ""
    prg_path = get_source_dir()
    # Ge contig info
    contigs = get_chrom_infos_from_fai(reference)
    try:
        with open(os.path.join(prg_path, "template.vcf"), "r") as template:
            for line in template:
                if line[:6] == "#CHROM":
                    lastline = line.replace("\n", "\t")
                    lastline += "\t".join(ordered_samples) + "\n"
                    continue
                vcf_header_str += line
        for ctg in sorted(contigs, key=lambda x: contigs[x], reverse=True):
            vcf_header_str += "##contig=<ID=%s,length=%d>\n" % (ctg,
                                                                contigs[ctg])
        vcf_header_str += lastline
    except IOError:
        raise ExecException("ERROR: template file not found in "
                            "program directory.")
    return vcf_header_str


def generate_samples_fastq(haploid, samples, output_dir, coverage, read_len,
                           insert_len_mean, insert_len_sd,
                           threads, stdout, stderr):
    printer = Print()
    elapsed_time = printer.elapsed_time()
    printer.out("Generating fastqfiles..."
                "%s seconds\n" % elapsed_time)

    prg_path = get_source_dir()
    # Generate reads for all individuals:
    cmd = [prg_path + "/pirs/pirs", "simulate", "-z", "-x", str(coverage), "--diploid", "-B",
           prg_path + "/pirs/Profiles/Base-Calling_Profiles/humNew.PE100.matrix.gz", "-I",
           prg_path + "/pirs/Profiles/InDel_Profiles/phixv2.InDel.matrix", "-l", str(read_len), "-m",
           str(insert_len_mean), "-v", str(insert_len_sd),
           "-G", prg_path + "/pirs/Profiles/GC-depth_Profiles/humNew.gcdep_100.dat", "-t", str(threads), "-o"]

    if haploid:
        cmd.remove("--diploid")
    for sample in samples:
        fasta_mat = sample.maternal.filename
        fasta_pat = sample.paternal.filename
        cmd_full = cmd + [output_dir, "-s", sample.ident]
        if haploid:
            cmd_full += [fasta_mat]
        else:
            cmd_full += [fasta_mat, fasta_pat]
        stdout_fh = open(stdout, "a") if stdout is not None else None
        stderr_fh = open(stderr, "a") if stderr is not None else None
        returncode = subprocess.call(cmd_full, stdout=stdout_fh, stderr=stderr_fh)
        if returncode != 0:
            printer = Print()
            printer.err("\nPirs exited with return code %d\n Exiting !" %
                        returncode)
            exit(1)


def init(output_dir, force_outputdir, sv_list, nstretches, nb_inds, reference,
         proba_del, proba_inv, proba_dup, del_allele_freq, inv_allele_freq,
         dup_allele_freq, haploid, force_polymorphism, no_reads,
         coverage, read_len, insert_len_mean, insert_len_sd, threads,
         genotypes=None, min_deletions=1, min_inversions=1, min_duplications=1,
         max_try=20, quiet=True, stdout=None, stderr=None, silent=False):  # MC
    """
    The main function:
       - initialisation of the Singleton Printer object
       - some parameter validation
       - generate_variant_graph, from scratch or from a vcf file
       - dumping the two fasta chromosomes for each individual
    """

    printer = Print(stdout=stdout, stderr=stderr, silent=silent)

    if not valid_params(output_dir, reference, nb_inds,
                        force_outputdir=force_outputdir):
        return 1

    output_vcf = os.path.join(output_dir, "genotypes.vcf")

    # set variant parameters
    variant_params = {
                      "min_del": min_deletions,
                      "min_inv": min_inversions,
                      "min_dup": min_duplications,
                      "proba_del": proba_del,
                      "proba_inv": proba_inv,
                      "proba_dup": proba_dup,
                      "del_allele_freq": del_allele_freq,
                      "inv_allele_freq": inv_allele_freq,
                      "dup_allele_freq": dup_allele_freq,
                      }

    vg, vp = generate_variant_graph(sv_list, nstretches, nb_inds, genotypes,
                                    variant_params, reference,
                                    output_dir, output_vcf,
                                    threads, max_try=max_try,
                                    stdout=stdout, stderr=stderr,
                                    quiet=quiet, silent=silent)

    dump_fastas_tofile(vg, vp, reference)
    elapsed_time = printer.elapsed_time()

    generate_samples_fastq(haploid, vp.ordered_samples(), output_dir,
                           coverage, read_len,
                           insert_len_mean, insert_len_sd,
                           threads, stdout, stderr)
    printer.out("Done...%s seconds\n" % elapsed_time)

    return 0


def valid_params(output_dir, reference, nb_inds, force_outputdir=False):
    """
    Checks the existence of output_dir, index file reference.fai and the
    validity of nb_inds (>1)
    """
    printer = Print()
    valid = True
    if os.path.isdir(output_dir):
        if force_outputdir:
            shutil.rmtree(output_dir)
        else:
            printer.err("Error: output directory {0} already exists.".format(output_dir))
            valid = False
    elif os.path.isfile(output_dir):
        printer.err("Error: unable to create output directory {0}: file exists.".format(output_dir))
        valid = False

    if nb_inds is not None and nb_inds < 2:
        printer.err("nb-inds must be at least 2")
        valid = False

    if not os.path.isfile(reference + ".fai"):
        os.system("samtools faidx " + reference)
    os.mkdir(output_dir)

    return valid


def main():
    args = parse_args()
    #for k, v in vars(args).items():
    #    print(k, v)
    #return
    init(**{k: v for k, v in vars(args).items() if k in init.__code__.co_varnames})


if __name__ == '__main__':
    sys.exit(main())
