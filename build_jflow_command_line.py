#!/usr/bin/env python

import argparse, os

parser = argparse.ArgumentParser(description='Generate simulated populations with SV')
parser.add_argument("--nb-inds", help="Number of individuals to generate", required=True)
parser.add_argument("--reference", help="Reference genome", required=True)
parser.add_argument("--prepare", help="Prepare workflow (default is detection workflow)", action='store_const', const=True, default=False)
parser.add_argument("--res-dir", help="Results dirs containing fastq reads files", default="res")
parser.add_argument("--chromosomes", help="Chromosomes list, comma separated", default=False)
parser.add_argument("--params", help="Other parameters for the workflow", default=False)
parser.add_argument("--bams-dir", help="Directory containing bams for each individual, created by cnvprepare", default=False)

args = parser.parse_args()

nb_inds = args.nb_inds
reference = os.path.abspath(args.reference)
bams_dir = args.bams_dir

if bams_dir:
    if bams_dir[-1] == "/":
        bams_dir = bams_dir[:-1]
    bams_dir = os.path.abspath(bams_dir)

if not args.prepare and not args.bams_dir:
    print("You must specify the directory containing the bam files (--bams-dir)")
    exit(1)

cmd = "/usr/local/bioinfo/src/Jflow/jflow-testing/bin/jflow_cli.py "

if args.prepare:
    cmd += "cnvprepare "
else:
    cmd += "cnvdetection "

cmd += "--reference-genome " + reference

if args.prepare:
    for i in range(1, int(nb_inds)+1):
        cmd += str(" --sample name=\"indiv{0}\" reads-1=\"" + args.res_dir + "/indiv{0}_1.fq.gz\" reads-2=\"" + \
               args.res_dir + "/indiv{0}_2.fq.gz\"").format(i)
else:
    for i in range(1, int(nb_inds)+1):
        cmd += " --sample name=\"indiv{0}\" alignments=\"{1}/indiv{0}-sorted-md.bam\"".format(i, bams_dir)

if args.chromosomes:
    cmd += " --chromosomes " + args.chromosomes

if args.params:
    cmd += " " + args.params

print(cmd)
