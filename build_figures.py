#!/usr/bin/env python3

import os
import sys
import tempfile
import shutil
import argparse
import glob
import pandas as pd
from PyPDF2 import PdfFileMerger, PdfFileReader
from numpy import isnan
from collections import OrderedDict
import matplotlib as mpl
mpl.use('Agg')  # Must be before seaborn import
import matplotlib.pyplot as pyplot
import seaborn as sns


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="Build figures")
    parser.add_argument('-i', '--input', type=str, required=False, help="Input directory containing tsv files",
                        default='.')
    parser.add_argument('-n', '--nb-inds', type=int, required=True, help='Number of individuals')
    parser.add_argument('-p', '--haploid', action='store_const', const=True, default=False, help='Is haploid')
    parser.add_argument('-s', '--stripplot', action='store_const', const=True, default=False,
                        help='Use stripplots instead of boxplots for genotype quality graphs')
    parser.add_argument('-o', '--output', type=str, required=False, help="Output directory", default='.')

    return parser.parse_args()


def __check_genotypes(nb_inds, haploid, col_names, tool, gt_qual_goods, gt_qual_bads):
    gt_file = glob.glob("*sv_genotypes_per_tools.tsv")[0]
    gt = pd.read_table(gt_file, index_col=0)

    for indiv in range(1, nb_inds + 1):
        gt_sv_col = "{0}__indiv{1}".format(tool, indiv)
        col_names.append(gt_sv_col)
        for i in range(0, len(gt["{0}__indiv{1}".format(tool, indiv)])):
            gt_sv = gt[gt_sv_col][i]
            gt_sv_ref = gt["Real_data__indiv{0}".format(indiv)][i]
            if (gt_sv == gt_sv_ref and not haploid) or \
                    (haploid and not isnan(gt_sv) and gt_sv.split("/")[0] == gt_sv_ref):
                gt_qual_bads[gt_sv_col].set_value(gt_qual_bads.index[i], pd.np.nan)
            else:
                gt_qual_goods[gt_sv_col].set_value(gt_qual_goods.index[i], pd.np.nan)


def __draw_gt_qual_figs(tool, tmp_dir, gt_qual, gt_qual_goods, gt_qual_bads, col_names, colors, colors_goods,
                       colors_bads, stripplot):
    fig, axes = pyplot.subplots(ncols=3, sharey=True, figsize=(18, 6))

    for ax in axes:
        ax.set_ylim(0, 105)

    # All:
    if not stripplot:
        plt = sns.boxplot(data=gt_qual[col_names], palette=colors, ax=axes[0])
    else:
        plt = sns.stripplot(data=gt_qual[col_names], palette=colors, ax=axes[0])
    plt.set(xticklabels=[str(i) for i in range(1, len(col_names) + 1)])
    plt.set_title(tool.title() + " [ALL]", fontsize=30)
    plt.set_ylabel("Quality (%)", fontsize=15)

    # Goods:
    if not stripplot:
        plt = sns.boxplot(data=gt_qual_goods[col_names], palette=colors_goods, ax=axes[1])
    else:
        plt = sns.stripplot(data=gt_qual_goods[col_names], palette=colors_goods, ax=axes[1])
    plt.set(xticklabels=[str(i) for i in range(1, len(col_names) + 1)])
    plt.set_title(tool.title() + " [GOODS]", fontsize=30)
    plt.set_xlabel("Samples", fontsize=15)

    # Bads:
    if not stripplot:
        plt = sns.boxplot(data=gt_qual_bads[col_names], palette=colors_bads, ax=axes[2])
    else:
        plt = sns.stripplot(data=gt_qual_bads[col_names], palette=colors_bads, ax=axes[2])
    plt.set(xticklabels=[str(i) for i in range(1, len(col_names) + 1)])
    plt.set_title(tool.title() + " [BADS]", fontsize=30)

    pyplot.savefig(os.path.join(tmp_dir, "gt_quality_" + tool + ".pdf"))


def __merge_quality_pdf_files(ltools, tmp_dir, output_dir):
    merger = PdfFileMerger()
    merger.addMetadata({u'/Title': u'Genotype quality for each individual'})
    page = 0
    for tool in ltools:
        pdf = os.path.join(tmp_dir, "gt_quality_" + tool + ".pdf")
        merger.append(PdfFileReader(open(pdf, 'rb')))
        merger.addBookmark(tool, page)
        page += 1
    merger.write(os.path.join(output_dir, "gt_quality.pdf"))


def build_genotype_quality_graphs(tools, nb_inds, haploid, stripplot, output_dir):
    tmp_dir = tempfile.mkdtemp()

    gt_quality_file = glob.glob("*sv_genotypes_quality_per_tools.tsv")[0]

    ltools = [] # Tools computed

    gt_qual = pd.read_table(gt_quality_file, index_col=0)
    gt_qual_goods = gt_qual.copy()
    gt_qual_bads = gt_qual.copy()

    with open(gt_quality_file, "r") as gt_quality_file_io:
        headers = gt_quality_file_io.readline().replace("\n", "").split("\t")

    # Genotypage:
    for tool in tools:
        if tool + "__indiv1" in headers:
            ltools.append(tool)
            col_names = []
            palette = sns.color_palette("RdYlGn", 100)

            __check_genotypes(nb_inds, haploid, col_names, tool, gt_qual_goods, gt_qual_bads)

            colors = [palette[int(round(gt_qual[x].mean()))] for x in col_names]
            colors_goods = [palette[int(round(gt_qual_goods[x].mean()))] for x in col_names]
            colors_bads = [palette[int(round(gt_qual_bads[x].mean())) if not isnan(gt_qual_bads[x].mean()) else 0] for x in col_names]

            #############
            # DRAW FIGS #
            #############
            __draw_gt_qual_figs(tool, tmp_dir, gt_qual, gt_qual_goods, gt_qual_bads, col_names, colors, colors_goods,
                               colors_bads, stripplot)

    # Merge quality files:
    __merge_quality_pdf_files(ltools, tmp_dir, output_dir)

    shutil.rmtree(tmp_dir)


def __compute_tp_fp_fn(svs, tool):
    tp = 0
    fp = 0
    fn = 0
    start_values = svs["{0}__Start".format(tool)]
    for i in range(0, len(start_values)):
        if isnan(start_values[i]) and not isnan(svs["Real_data__Start"][i]):
            fn += 1
        elif not isnan(start_values[i]) and not isnan(svs["Real_data__Start"][i]):
            tp += 1
        elif not isnan(start_values[i]) and isnan(svs["Real_data__Start"][i]):
            fp += 1
    return tp, fp, fn


def __draw_recall_fig(recall, output_dir):
    recall_df = pd.DataFrame.from_dict(recall, orient="columns")
    plt = sns.barplot(data=recall_df)
    plt.set_title("Recall", fontsize=30)
    plt.set_ylabel("Recall (%)", fontsize=15)
    plt.set_xlabel("Tool", fontsize=15)
    plt.tick_params(labelsize=8)
    plt.set_ylim([0,100])
    pyplot.savefig(os.path.join(output_dir, "recall_per_tool.pdf"))


def __draw_precision_fig(precision, output_dir):
    precision_df = pd.DataFrame.from_dict(precision, orient="columns")
    plt = sns.barplot(data=precision_df)
    plt.set_title("Precision", fontsize=30)
    plt.set_ylabel("Precision (%)", fontsize=15)
    plt.set_xlabel("Tool", fontsize=15)
    plt.tick_params(labelsize=8)
    pyplot.savefig(os.path.join(output_dir, "precision_per_tool.pdf"))


def build_recall_precision_graphs(tools, output_dir):
    svs_file = glob.glob("*sv_per_tools.tsv")[0]

    # Precision / Recall:
    with open(svs_file, "r") as sv_file_io:
        sv_headers = sv_file_io.readline().replace("\n", "").split("\t")

    svs = pd.read_table(svs_file, index_col=0)

    recall = OrderedDict()
    precision = OrderedDict()

    for tool in tools:
        if tool + "__Start" in sv_headers:
            tp, fp, fn = __compute_tp_fp_fn(svs, tool)
            recall[tool] = [tp / (tp+fn) * 100]
            precision[tool] = [tp / (tp+fp) * 100]

    pyplot.subplots(ncols=1)  # Reset to one figure

    # Recall:
    __draw_recall_fig(recall, output_dir)

    # Precision:
    __draw_precision_fig(precision, output_dir)


def init(input_dir, nb_inds, haploid, stripplot, output):
    os.chdir(input_dir)
    tools = ["breakdancer", "delly", "lumpy", "pindel", "cnvnator", "genomestrip", "Filtered_results"]
    build_recall_precision_graphs(tools, output)
    if len(glob.glob("*sv_genotypes_quality_per_tools.tsv")) > 0:
        build_genotype_quality_graphs(tools, nb_inds, haploid, stripplot, output)
    else:
        print("No genotypes found. Step ignored.")


def main():
    args = parse_args()
    output = os.path.abspath(args.output) if args.output != "." else os.getcwd()
    if not os.path.exists(output):
        os.makedirs(output)
    init(args.input, args.nb_inds, args.haploid, args.stripplot, output)


if __name__ == '__main__':
    sys.exit(main())
